/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/3/29 11:26:33                           */
/*==============================================================*/


drop table if exists job;

drop table if exists job_duty;

drop table if exists job_type;

drop table if exists joiner;

drop table if exists level;

drop table if exists org_peron;

drop table if exists organization;

drop table if exists people;

/*==============================================================*/
/* Table: job                                                   */
/*==============================================================*/
create table job
(
   job_id               int not null,
   org_id               int,
   job_type             varchar(20),
   job_no               int,
   job_name             varchar(20),
   job_describe         char(10),
   job_start            char(10),
   job_end              char(10),
   job_salary           char(10),
   job_state            varchar(20),
   job_create           time,
   job_update           time,
   primary key (job_id)
);

/*==============================================================*/
/* Table: job_duty                                              */
/*==============================================================*/
create table job_duty
(
   duty_id              int not null,
   job_id               int,
   duty_duty            varchar(1024),
   duty_create          time,
   duty_update          time,
   primary key (duty_id)
);

/*==============================================================*/
/* Table: job_type                                              */
/*==============================================================*/
create table job_type
(
   job_type             varchar(20),
   job_no               int,
   job_name             varchar(20),
   job_des              varchar(200),
   job_state            varchar(20),
   job_create           time,
   job_update           time
);

/*==============================================================*/
/* Table: joiner                                                */
/*==============================================================*/
create table joiner
(
   join_id              int not null,
   join_name            varchar(20),
   join_isin            varchar(20),
   join_state           varchar(20),
   join_type            varchar(20),
   join_create          time,
   join_update          time,
   primary key (join_id)
);

/*==============================================================*/
/* Table: level                                                 */
/*==============================================================*/
create table level
(
   lev_id               int not null,
   lev_name             varchar(20),
   lev_no               varchar(20),
   lev_remark           varchar(100),
   lev_creat            time,
   lev_update           time,
   primary key (lev_id)
);

/*==============================================================*/
/* Table: org_peron                                             */
/*==============================================================*/
create table org_peron
(
   org_person_posi      char(10),
   peo_id               char(10),
   job_id               char(10),
   org_id               char(10),
   org_person_start     char(10),
   org_person_end       char(10)
);

/*==============================================================*/
/* Table: organization                                          */
/*==============================================================*/
create table organization
(
   org_id               int not null,
   join_id              int,
   org_name             varchar(20),
   org_no               int,
   org_father_id        int,
   org_admin_id         int,
   org_adress           varchar(200),
   org_staff_id         int,
   org_remark           varchar(200),
   org_create           time,
   org_update           time,
   primary key (org_id)
);

/*==============================================================*/
/* Table: people                                                */
/*==============================================================*/
create table people
(
   peo_no               int not null,
   peo_lev_id           int,
   peo_joiner_id        int,
   peo_name             varchar(20),
   peo_sex              varchar(2),
   peo_head             varchar(20),
   primary key (peo_no)
);

