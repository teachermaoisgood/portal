/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/3/29 11:12:46                           */
/*==============================================================*/


alter table Appmodle
   drop primary key;

drop table if exists Appmodle;

alter table child_sys
   drop primary key;

drop table if exists child_sys;

alter table fun_per_res
   drop primary key;

drop table if exists fun_per_res;

alter table role
   drop primary key;

drop table if exists role;

alter table role_right
   drop primary key;

drop table if exists role_right;

alter table user_acc
   drop primary key;

drop table if exists user_acc;

alter table user_login
   drop primary key;

drop table if exists user_login;

alter table user_login_history
   drop primary key;

drop table if exists user_login_history;

alter table user_role
   drop primary key;

drop table if exists user_role;

alter table users
   drop primary key;

drop table if exists users;

alter table users_role
   drop primary key;

drop table if exists users_role;

/*==============================================================*/
/* Table: Appmodle                                              */
/*==============================================================*/
create table Appmodle
(
   modle_id             int primary key auto_increment,
   app_id               int,
   modle_name           varchar(20),
   modle_code           varchar(200),
   modle_img_url        varchar(200),
   modle_in_url         varchar(200),
   modle_isadmin        varchar(200),
   modle_order          int,
   model_state          varchar(20),
   model_create         time,
   model_update         time
);

alter table Appmodle
   add primary key (modle_id);

/*==============================================================*/
/* Table: child_sys                                             */
/*==============================================================*/
create table child_sys
(
   app_id               int primary key auto_increment,
   app_name             varchar(20),
   app_code             varchar(200),
   app_img_url          varchar(200),
   app_in_url           varchar(200),
   app_developers       varchar(20),
   app_isadmin          varchar(20),
   app_order            int,
   app_state            varchar(20),
   app_create           time,
   app_update           time
);

alter table child_sys
   add primary key (app_id);

/*==============================================================*/
/* Table: fun_per_res                                           */
/*==============================================================*/
create table fun_per_res
(
   res_id               int primary key auto_increment,
   modle_id             int,
   res_name             varchar(20),
   res_code             varchar(200),
   res_type             varchar(20),
   res_img_url          varchar(200),
   res_in_url           varchar(200),
   res_orde             int,
   res_father_id        int,
   res_state            varchar(20),
   res_create           time,
   res_update           time
);

alter table fun_per_res
   add primary key (res_id);

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   role_id              int primary key auto_increment,
   role_name            varchar(20),
   role_code            varchar(200),
   role_des             varchar(200),
   role_order           int,
   role_state           varchar(20),
   role_create          time,
   role_update          time
);

alter table role
   add primary key (role_id);

/*==============================================================*/
/* Table: role_right                                            */
/*==============================================================*/
create table role_right
(
   ROLE_RIGHT_ID        int primary key auto_increment,
   res_id               int,
   role_id              int
);

alter table role_right
   add primary key (ROLE_RIGHT_ID);

/*==============================================================*/
/* Table: user_acc                                              */
/*==============================================================*/
create table user_acc
(
   users_userlogin      int primary key auto_increment,
   user_id              int,
   users_id             int
);

alter table user_acc
   add primary key (users_userlogin);

/*==============================================================*/
/* Table: user_login                                            */
/*==============================================================*/
create table user_login
(
   user_id              int primary key auto_increment,
   user_user            varchar(20),
   user_pass            varchar(20),
   user_pass_time       int,
   user_name            varchar(20),
   user_email           varchar(20),
   user_tel             varchar(20),
   user_issysacc        varchar(200),
   user_isable          varchar(200),
   user_isunlogin       bool,
   user_pass_need_edit  blob,
   user_login_address   varchar(200),
   user_login_time      varchar(200),
   user_ban_time        time,
   user_ban_name        varchar(20),
   user_fail_time       int,
   user_state           varchar(20),
   user_create          time,
   user_update          time
);

alter table user_login
   add primary key (user_id);

/*==============================================================*/
/* Table: user_login_history                                    */
/*==============================================================*/
create table user_login_history
(
   history_id           int not null,
   user_id              int,
   history_start        time,
   history_end          time,
   history_user         varchar(20),
   history_pass         varchar(20),
   history_suc          varchar(2),
   history_create       time,
   history_update       time
);

alter table user_login_history
   add primary key (history_id);

/*==============================================================*/
/* Table: user_role                                             */
/*==============================================================*/
create table user_role
(
   role_party_id        int primary key auto_increment,
   role_id              int,
   user_d               int
);

alter table user_role
   add primary key (role_party_id);

/*==============================================================*/
/* Table: users                                                 */
/*==============================================================*/
create table users
(
   users_id             int primary key auto_increment,
   users_name           varchar(20),
   users_no             varchar(20),
   users_create         time,
   users_update         time
);

alter table users
   add primary key (users_id);

/*==============================================================*/
/* Table: users_role                                            */
/*==============================================================*/
create table users_role
(
   user_users_role_     int primary key auto_increment,
   users_id             int,
   role_id              int
);

alter table users_role
   add primary key (user_users_role_);

