package com.hbxy.tg.portal.admin.org.Service;

import com.hbxy.tg.portal.admin.org.model.UserLogin;
import com.hbxy.tg.portal.admin.org.model.UserLoginExample;
import org.apache.ibatis.annotations.Param;
import com.hbxy.tg.portal.admin.org.model.UserLogin;

import java.util.List;

public interface UserLoginService {
    long countByExample(UserLoginExample example);

    int deleteByExample(UserLoginExample example);

    int deleteByPrimaryKey(Integer userId);

    int insert(UserLogin record);

    int insertSelective(UserLogin record);

    List<UserLogin> selectByExampleWithBLOBs(UserLoginExample example);

    List<UserLogin> selectByExample(UserLoginExample example);
    List<UserLogin> selectRoleAll(Integer userId);
    List<UserLogin> selectByIssysacc(UserLoginExample example);
    UserLogin selectByPrimaryKey(Integer userId);

    int updateByExampleSelective(@Param("record") UserLogin record, @Param("example") UserLoginExample example);

    int updateByExampleWithBLOBs(@Param("record") UserLogin record, @Param("example") UserLoginExample example);

    int updateByExample(@Param("record") UserLogin record, @Param("example") UserLoginExample example);

    int updateByPrimaryKeySelective(UserLogin record);


    int updateByPrimaryKey(UserLogin record);
}
