package com.hbxy.tg.portal.admin.right.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RoleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public RoleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andRoleIdIsNull() {
            addCriterion("role_id is null");
            return (Criteria) this;
        }

        public Criteria andRoleIdIsNotNull() {
            addCriterion("role_id is not null");
            return (Criteria) this;
        }

        public Criteria andRoleIdEqualTo(Integer value) {
            addCriterion("role_id =", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotEqualTo(Integer value) {
            addCriterion("role_id <>", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdGreaterThan(Integer value) {
            addCriterion("role_id >", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("role_id >=", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdLessThan(Integer value) {
            addCriterion("role_id <", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdLessThanOrEqualTo(Integer value) {
            addCriterion("role_id <=", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdIn(List<Integer> values) {
            addCriterion("role_id in", values, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotIn(List<Integer> values) {
            addCriterion("role_id not in", values, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdBetween(Integer value1, Integer value2) {
            addCriterion("role_id between", value1, value2, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("role_id not between", value1, value2, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleNameIsNull() {
            addCriterion("role_name is null");
            return (Criteria) this;
        }

        public Criteria andRoleNameIsNotNull() {
            addCriterion("role_name is not null");
            return (Criteria) this;
        }

        public Criteria andRoleNameEqualTo(String value) {
            addCriterion("role_name =", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotEqualTo(String value) {
            addCriterion("role_name <>", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameGreaterThan(String value) {
            addCriterion("role_name >", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameGreaterThanOrEqualTo(String value) {
            addCriterion("role_name >=", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameLessThan(String value) {
            addCriterion("role_name <", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameLessThanOrEqualTo(String value) {
            addCriterion("role_name <=", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameLike(String value) {
            addCriterion("role_name like", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotLike(String value) {
            addCriterion("role_name not like", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameIn(List<String> values) {
            addCriterion("role_name in", values, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotIn(List<String> values) {
            addCriterion("role_name not in", values, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameBetween(String value1, String value2) {
            addCriterion("role_name between", value1, value2, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotBetween(String value1, String value2) {
            addCriterion("role_name not between", value1, value2, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleCodeIsNull() {
            addCriterion("role_code is null");
            return (Criteria) this;
        }

        public Criteria andRoleCodeIsNotNull() {
            addCriterion("role_code is not null");
            return (Criteria) this;
        }

        public Criteria andRoleCodeEqualTo(String value) {
            addCriterion("role_code =", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeNotEqualTo(String value) {
            addCriterion("role_code <>", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeGreaterThan(String value) {
            addCriterion("role_code >", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeGreaterThanOrEqualTo(String value) {
            addCriterion("role_code >=", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeLessThan(String value) {
            addCriterion("role_code <", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeLessThanOrEqualTo(String value) {
            addCriterion("role_code <=", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeLike(String value) {
            addCriterion("role_code like", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeNotLike(String value) {
            addCriterion("role_code not like", value, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeIn(List<String> values) {
            addCriterion("role_code in", values, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeNotIn(List<String> values) {
            addCriterion("role_code not in", values, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeBetween(String value1, String value2) {
            addCriterion("role_code between", value1, value2, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleCodeNotBetween(String value1, String value2) {
            addCriterion("role_code not between", value1, value2, "roleCode");
            return (Criteria) this;
        }

        public Criteria andRoleDesIsNull() {
            addCriterion("role_des is null");
            return (Criteria) this;
        }

        public Criteria andRoleDesIsNotNull() {
            addCriterion("role_des is not null");
            return (Criteria) this;
        }

        public Criteria andRoleDesEqualTo(String value) {
            addCriterion("role_des =", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesNotEqualTo(String value) {
            addCriterion("role_des <>", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesGreaterThan(String value) {
            addCriterion("role_des >", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesGreaterThanOrEqualTo(String value) {
            addCriterion("role_des >=", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesLessThan(String value) {
            addCriterion("role_des <", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesLessThanOrEqualTo(String value) {
            addCriterion("role_des <=", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesLike(String value) {
            addCriterion("role_des like", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesNotLike(String value) {
            addCriterion("role_des not like", value, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesIn(List<String> values) {
            addCriterion("role_des in", values, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesNotIn(List<String> values) {
            addCriterion("role_des not in", values, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesBetween(String value1, String value2) {
            addCriterion("role_des between", value1, value2, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleDesNotBetween(String value1, String value2) {
            addCriterion("role_des not between", value1, value2, "roleDes");
            return (Criteria) this;
        }

        public Criteria andRoleOrderIsNull() {
            addCriterion("role_order is null");
            return (Criteria) this;
        }

        public Criteria andRoleOrderIsNotNull() {
            addCriterion("role_order is not null");
            return (Criteria) this;
        }

        public Criteria andRoleOrderEqualTo(Integer value) {
            addCriterion("role_order =", value, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderNotEqualTo(Integer value) {
            addCriterion("role_order <>", value, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderGreaterThan(Integer value) {
            addCriterion("role_order >", value, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("role_order >=", value, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderLessThan(Integer value) {
            addCriterion("role_order <", value, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderLessThanOrEqualTo(Integer value) {
            addCriterion("role_order <=", value, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderIn(List<Integer> values) {
            addCriterion("role_order in", values, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderNotIn(List<Integer> values) {
            addCriterion("role_order not in", values, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderBetween(Integer value1, Integer value2) {
            addCriterion("role_order between", value1, value2, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("role_order not between", value1, value2, "roleOrder");
            return (Criteria) this;
        }

        public Criteria andRoleStateIsNull() {
            addCriterion("role_state is null");
            return (Criteria) this;
        }

        public Criteria andRoleStateIsNotNull() {
            addCriterion("role_state is not null");
            return (Criteria) this;
        }

        public Criteria andRoleStateEqualTo(String value) {
            addCriterion("role_state =", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateNotEqualTo(String value) {
            addCriterion("role_state <>", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateGreaterThan(String value) {
            addCriterion("role_state >", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateGreaterThanOrEqualTo(String value) {
            addCriterion("role_state >=", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateLessThan(String value) {
            addCriterion("role_state <", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateLessThanOrEqualTo(String value) {
            addCriterion("role_state <=", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateLike(String value) {
            addCriterion("role_state like", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateNotLike(String value) {
            addCriterion("role_state not like", value, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateIn(List<String> values) {
            addCriterion("role_state in", values, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateNotIn(List<String> values) {
            addCriterion("role_state not in", values, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateBetween(String value1, String value2) {
            addCriterion("role_state between", value1, value2, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleStateNotBetween(String value1, String value2) {
            addCriterion("role_state not between", value1, value2, "roleState");
            return (Criteria) this;
        }

        public Criteria andRoleCreateIsNull() {
            addCriterion("role_create is null");
            return (Criteria) this;
        }

        public Criteria andRoleCreateIsNotNull() {
            addCriterion("role_create is not null");
            return (Criteria) this;
        }

        public Criteria andRoleCreateEqualTo(Date value) {
            addCriterionForJDBCTime("role_create =", value, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateNotEqualTo(Date value) {
            addCriterionForJDBCTime("role_create <>", value, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateGreaterThan(Date value) {
            addCriterionForJDBCTime("role_create >", value, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("role_create >=", value, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateLessThan(Date value) {
            addCriterionForJDBCTime("role_create <", value, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("role_create <=", value, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateIn(List<Date> values) {
            addCriterionForJDBCTime("role_create in", values, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateNotIn(List<Date> values) {
            addCriterionForJDBCTime("role_create not in", values, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("role_create between", value1, value2, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleCreateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("role_create not between", value1, value2, "roleCreate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateIsNull() {
            addCriterion("role_update is null");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateIsNotNull() {
            addCriterion("role_update is not null");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateEqualTo(Date value) {
            addCriterionForJDBCTime("role_update =", value, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateNotEqualTo(Date value) {
            addCriterionForJDBCTime("role_update <>", value, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateGreaterThan(Date value) {
            addCriterionForJDBCTime("role_update >", value, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("role_update >=", value, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateLessThan(Date value) {
            addCriterionForJDBCTime("role_update <", value, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("role_update <=", value, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateIn(List<Date> values) {
            addCriterionForJDBCTime("role_update in", values, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateNotIn(List<Date> values) {
            addCriterionForJDBCTime("role_update not in", values, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("role_update between", value1, value2, "roleUpdate");
            return (Criteria) this;
        }

        public Criteria andRoleUpdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("role_update not between", value1, value2, "roleUpdate");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}