package com.hbxy.tg.portal.admin.login.logincontroller;

import com.hbxy.tg.portal.admin.org.Service.UserLoginService;
import com.hbxy.tg.portal.admin.org.model.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import javax.servlet.http.HttpSession;

@Controller
public class Logincontroller {
    @Autowired
    UserLoginService userLoginService;
    public static Integer userId;
    public static String  Issysacc;
    @RequestMapping(value="/login",method = RequestMethod.GET)
    public String toLogin(){
        return "login";
    }
    @RequestMapping(value="/login",method = RequestMethod.POST)
    public String Login(UserLogin user, Model model, HttpSession session) {
        userId = user.getUserId();
        String pass = user.getUserPass();

        if (userId != null && pass != null) {
            UserLogin userLogin = userLoginService.selectByPrimaryKey(userId);
            if (userLogin != null) {
                Issysacc = userLogin.getUserIssysacc();

                if (pass.equals(userLogin.getUserPass())) {
                    session.setAttribute("USER_SESSION", user);
                    return "redirect:back";
                } else {
                    model.addAttribute("message", "≡(▔﹏▔)≡用户名或密码错误请检查");
                    return "login";
                }
            } else {
                model.addAttribute("message", "≡(▔﹏▔)≡用户名不存在");
                return "login";
            }

        } else {
            model.addAttribute("message", "≡(▔﹏▔)≡用户名与密码不能为空");
            return "login";
        }



    }
    @RequestMapping("/back")
    public String toHome(Model model){
        model.addAttribute("userss",Issysacc);

        model.addAttribute("username",userId);
        return "back";
    }
    @RequestMapping("/logout")
    public String logout(HttpSession session, Model model) {
        model.addAttribute("message", "≡(▔﹏▔)≡请您登录");
        session.invalidate();
        return "login";
    }




}
