package com.hbxy.tg.portal.admin.app.controller;

import com.hbxy.tg.portal.admin.app.model.Appmodle;
import com.hbxy.tg.portal.admin.app.model.AppmodleExample;
import com.hbxy.tg.portal.admin.app.model.ChildSys;
import com.hbxy.tg.portal.admin.app.model.ChildSysExample;
import com.hbxy.tg.portal.admin.app.service.AppmodleService;
import com.hbxy.tg.portal.admin.app.service.ChildSysService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@Controller
@RequestMapping("/app")
public class ChildSysController {
        @Autowired
        ChildSysService childSysService;
//        @Autowired
//        AppmodleService appmodleService;
        //初始列表
    @RequestMapping("/apptable")
    @ResponseBody
    public JSONObject apptable() {
        JSONObject json = new JSONObject();
        JSONObject jsondata = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1);
        List<ChildSys> list = childSysService.selectAll();
        for (int i = 0; i < list.size(); i++) {
            jsondata.put("appId", list.get(i).getAppId());
            jsondata.put("appName", list.get(i).getAppName());
            jsondata.put("appState", list.get(i).getAppState());
            jsondata.put("appDevelopers", list.get(i).getAppDevelopers());
            jsonArray.add(jsondata);
            json.put("data", jsonArray);
        }
        return json;
    }
        //应用添加
        @RequestMapping("/addapp")
        @ResponseBody
        public Integer addapp(@RequestBody ChildSys childSys){
        System.out.println(childSys.toString());
            childSysService.insert(childSys);
            return 1;
        }
        //应用修改
        @RequestMapping( "/appedit")
        @ResponseBody
        public Integer appedit(@RequestBody ChildSys childSys){
            System.out.println(childSys.toString());
            childSysService.updateByPrimaryKey(childSys);
            return 1;
        }
        //    删除
        @RequestMapping( "/appdel" )
        @ResponseBody
        public Integer appdel(@RequestBody ChildSys childSys){
            childSysService.deleteByPrimaryKey(childSys.getAppId());
            return 1;
        }
//        //    模块管理
//        @RequestMapping("/modList")
//        @ResponseBody
//        public List<Appmodle> modeList(@PathVariable("name") String name)throws Exception{
//
//
//            AppmodleExample example=new AppmodleExample();
//            example.createCriteria();
//            List<Appmodle> childmodes=appmodleService.selectByExample(example);
//            for(Appmodle a:childmodes){
//                a.getModleName();
//            }
//
//            return childmodes;
//        }
//        //    模块添加
//        @RequestMapping(value = "/modAdd",method = RequestMethod.POST)
//        @ResponseBody
//        public Appmodle modAdd(@RequestBody Appmodle mode){
//            appmodleService.insert(mode);
//            return mode;
//        }
//        //    模块修改
//        @RequestMapping(value = "/modEdit",method = RequestMethod.PUT)
//        @ResponseBody
//        public Appmodle modEdit(@RequestBody Appmodle mode){
//            appmodleService.updateByPrimaryKeySelective(mode);
//            return mode;
//        }
//        //    模块删除
//        @RequestMapping(value = "/modDel",method =RequestMethod.DELETE )
//        @ResponseBody
//        public String modDel(@RequestBody Appmodle mode){
//            appmodleService.deleteByPrimaryKey(mode.getModleId());
//            return "200";
//        }
    }


