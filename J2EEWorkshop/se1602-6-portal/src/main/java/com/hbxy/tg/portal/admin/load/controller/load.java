package com.hbxy.tg.portal.admin.load.controller;

import com.hbxy.tg.portal.admin.login.logincontroller.Logincontroller;
import com.hbxy.tg.portal.admin.org.Service.UserLoginService;
import com.hbxy.tg.portal.admin.org.model.UserLogin;
import com.hbxy.tg.portal.admin.right.model.Role;
import com.hbxy.tg.portal.admin.right.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class load {
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserLoginService userLoginService;


    @RequestMapping("/load")
    public String loadorg(String page , Model model) {
        

        switch (page){
            case "org":
                return "/orgAndPeople/org";
            case "user":
                
                return "/orgAndPeople/people";
            case "job":
                return "/orgAndPeople/job";
            case "lev":
                return "/orgAndPeople/joblevel";
            case "app":
                return "/app/app";
            case "addapp":
                return "/app/addapp";


            case "model":
                return "/app/model";
            case "addmodel":
                return "/app/addmod";
            case "addrole":
                return "/right/addrole";
            case "role":
                 return "/right/role";
            case "right":
                 return "/right/role_right";
            case "peo":
                System.out.println(Logincontroller.userId);
                UserLogin u =userLoginService.selectByPrimaryKey(Logincontroller.userId);
                model.addAttribute("user",u);
                System.out.println(u.toString());
                return "/user/showUser";
            case "peo_role":
                List<Role> list=roleService.selectAll();

                model.addAttribute("rolelist",list);
                return "/orgAndPeople/"+page;
            case "editpass":
                  return "/user/editpass";
            case "addjob":
                return "/orgAndPeople/"+page;
            case "addjoblevel":
                return "/orgAndPeople/"+page;
            case "addorg":
                return "/orgAndPeople/"+page;
            case "adduser":
                return "/orgAndPeople/"+page;
        }
        return "";
    }

}
