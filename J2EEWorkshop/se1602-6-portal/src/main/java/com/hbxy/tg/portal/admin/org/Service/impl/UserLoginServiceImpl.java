package com.hbxy.tg.portal.admin.org.Service.impl;

import com.hbxy.tg.portal.admin.org.Service.UserLoginService;
import com.hbxy.tg.portal.admin.org.mapper.UserLoginDAO;
import com.hbxy.tg.portal.admin.org.model.UserLogin;
import com.hbxy.tg.portal.admin.org.model.UserLoginExample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserLoginServiceImpl implements UserLoginService {
    @Autowired
    private UserLoginDAO mapper;

    public long countByExample(UserLoginExample example) {
        return mapper.countByExample(example);
    }

    public int deleteByExample(UserLoginExample example) {
        return mapper.deleteByExample(example);
    }

    public int deleteByPrimaryKey(Integer userId) {
        return mapper.deleteByPrimaryKey(userId);
    }

    public int insert(UserLogin record) {
        return mapper.insert(record);
    }

    public int insertSelective(UserLogin record) {
        return mapper.insertSelective(record);
    }

    public List<UserLogin> selectByExampleWithBLOBs(UserLoginExample example) {
        return mapper.selectByExampleWithBLOBs(example);
    }

    public List<UserLogin> selectByExample(UserLoginExample example) {
        return mapper.selectByExample(example);
    }

    @Override
    public List<UserLogin> selectRoleAll(Integer userId) {
        return mapper.selectRoleAll(userId);
    }

    @Override
    public List<UserLogin> selectByIssysacc(UserLoginExample example) {
        return mapper.selectByIssysacc(example);
    }

    public UserLogin selectByPrimaryKey(Integer userId) {
        return mapper.selectByPrimaryKey(userId);
    }

    @Override
    public int updateByExampleSelective(UserLogin record, UserLoginExample example) {
        return mapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExampleWithBLOBs(UserLogin record, UserLoginExample example) {
        return mapper.updateByExampleWithBLOBs(record, example);
    }

    @Override
    public int updateByExample(UserLogin record, UserLoginExample example) {
        return mapper.updateByExample(record, example);
    }



    public int updateByPrimaryKeySelective(UserLogin record) {
        return mapper.updateByPrimaryKeySelective(record);
    }



    public int updateByPrimaryKeyWithBLOBs(UserLogin record) {
        return mapper.updateByPrimaryKeyWithBLOBs(record);
    }

    public int updateByPrimaryKey(UserLogin record) {
        return mapper.updateByPrimaryKey(record);
    }

}
