package com.hbxy.tg.portal.admin.app.model;

import java.io.Serializable;
import java.util.Date;

/**
 * child_sys
 * @author 
 */
public class ChildSys implements Serializable {
    private Integer appId;

    private String appName;

    private String appCode;

    private String appImgUrl;

    private String appInUrl;

    private String appDevelopers;

    private String appIsadmin;

    private Integer appOrder;

    private String appState;

    private Date appCreate;

    private Date appUpdate;

    private static final long serialVersionUID = 1L;

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getAppImgUrl() {
        return appImgUrl;
    }

    public void setAppImgUrl(String appImgUrl) {
        this.appImgUrl = appImgUrl;
    }

    public String getAppInUrl() {
        return appInUrl;
    }

    public void setAppInUrl(String appInUrl) {
        this.appInUrl = appInUrl;
    }

    public String getAppDevelopers() {
        return appDevelopers;
    }

    public void setAppDevelopers(String appDevelopers) {
        this.appDevelopers = appDevelopers;
    }

    public String getAppIsadmin() {
        return appIsadmin;
    }

    public void setAppIsadmin(String appIsadmin) {
        this.appIsadmin = appIsadmin;
    }

    public Integer getAppOrder() {
        return appOrder;
    }

    public void setAppOrder(Integer appOrder) {
        this.appOrder = appOrder;
    }

    public String getAppState() {
        return appState;
    }

    public void setAppState(String appState) {
        this.appState = appState;
    }

    @Override
    public String toString() {
        return "ChildSys{" +
                "appId=" + appId +
                ", appName='" + appName + '\'' +
                ", appCode='" + appCode + '\'' +
                ", appImgUrl='" + appImgUrl + '\'' +
                ", appInUrl='" + appInUrl + '\'' +
                ", appDevelopers='" + appDevelopers + '\'' +
                ", appIsadmin='" + appIsadmin + '\'' +
                ", appOrder=" + appOrder +
                ", appState='" + appState + '\'' +
                ", appCreate=" + appCreate +
                ", appUpdate=" + appUpdate +
                '}';
    }

    public Date getAppCreate() {
        return appCreate;
    }

    public void setAppCreate(Date appCreate) {
        this.appCreate = appCreate;
    }

    public Date getAppUpdate() {
        return appUpdate;
    }

    public void setAppUpdate(Date appUpdate) {
        this.appUpdate = appUpdate;
    }
}