package com.hbxy.tg.portal.admin.right.model;

import java.io.Serializable;

/**
 * role_right
 * @author 
 */
public class RoleRight implements Serializable {
    private Integer roleRightId;

    private Integer resId;//right

    private Integer roleId;

    private static final long serialVersionUID = 1L;

    public Integer getRoleRightId() {
        return roleRightId;
    }

    public void setRoleRightId(Integer roleRightId) {
        this.roleRightId = roleRightId;
    }

    public Integer getResId() {
        return resId;
    }

    public void setResId(Integer resId) {
        this.resId = resId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}