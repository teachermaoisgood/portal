package com.hbxy.tg.portal.admin.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AppmodleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public AppmodleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andModleIdIsNull() {
            addCriterion("modle_id is null");
            return (Criteria) this;
        }

        public Criteria andModleIdIsNotNull() {
            addCriterion("modle_id is not null");
            return (Criteria) this;
        }

        public Criteria andModleIdEqualTo(Integer value) {
            addCriterion("modle_id =", value, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdNotEqualTo(Integer value) {
            addCriterion("modle_id <>", value, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdGreaterThan(Integer value) {
            addCriterion("modle_id >", value, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("modle_id >=", value, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdLessThan(Integer value) {
            addCriterion("modle_id <", value, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdLessThanOrEqualTo(Integer value) {
            addCriterion("modle_id <=", value, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdIn(List<Integer> values) {
            addCriterion("modle_id in", values, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdNotIn(List<Integer> values) {
            addCriterion("modle_id not in", values, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdBetween(Integer value1, Integer value2) {
            addCriterion("modle_id between", value1, value2, "modleId");
            return (Criteria) this;
        }

        public Criteria andModleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("modle_id not between", value1, value2, "modleId");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNull() {
            addCriterion("app_id is null");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNotNull() {
            addCriterion("app_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdEqualTo(Integer value) {
            addCriterion("app_id =", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotEqualTo(Integer value) {
            addCriterion("app_id <>", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThan(Integer value) {
            addCriterion("app_id >", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("app_id >=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThan(Integer value) {
            addCriterion("app_id <", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThanOrEqualTo(Integer value) {
            addCriterion("app_id <=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdIn(List<Integer> values) {
            addCriterion("app_id in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotIn(List<Integer> values) {
            addCriterion("app_id not in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdBetween(Integer value1, Integer value2) {
            addCriterion("app_id between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotBetween(Integer value1, Integer value2) {
            addCriterion("app_id not between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andModleNameIsNull() {
            addCriterion("modle_name is null");
            return (Criteria) this;
        }

        public Criteria andModleNameIsNotNull() {
            addCriterion("modle_name is not null");
            return (Criteria) this;
        }

        public Criteria andModleNameEqualTo(String value) {
            addCriterion("modle_name =", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameNotEqualTo(String value) {
            addCriterion("modle_name <>", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameGreaterThan(String value) {
            addCriterion("modle_name >", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameGreaterThanOrEqualTo(String value) {
            addCriterion("modle_name >=", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameLessThan(String value) {
            addCriterion("modle_name <", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameLessThanOrEqualTo(String value) {
            addCriterion("modle_name <=", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameLike(String value) {
            addCriterion("modle_name like", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameNotLike(String value) {
            addCriterion("modle_name not like", value, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameIn(List<String> values) {
            addCriterion("modle_name in", values, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameNotIn(List<String> values) {
            addCriterion("modle_name not in", values, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameBetween(String value1, String value2) {
            addCriterion("modle_name between", value1, value2, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleNameNotBetween(String value1, String value2) {
            addCriterion("modle_name not between", value1, value2, "modleName");
            return (Criteria) this;
        }

        public Criteria andModleCodeIsNull() {
            addCriterion("modle_code is null");
            return (Criteria) this;
        }

        public Criteria andModleCodeIsNotNull() {
            addCriterion("modle_code is not null");
            return (Criteria) this;
        }

        public Criteria andModleCodeEqualTo(String value) {
            addCriterion("modle_code =", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeNotEqualTo(String value) {
            addCriterion("modle_code <>", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeGreaterThan(String value) {
            addCriterion("modle_code >", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeGreaterThanOrEqualTo(String value) {
            addCriterion("modle_code >=", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeLessThan(String value) {
            addCriterion("modle_code <", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeLessThanOrEqualTo(String value) {
            addCriterion("modle_code <=", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeLike(String value) {
            addCriterion("modle_code like", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeNotLike(String value) {
            addCriterion("modle_code not like", value, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeIn(List<String> values) {
            addCriterion("modle_code in", values, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeNotIn(List<String> values) {
            addCriterion("modle_code not in", values, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeBetween(String value1, String value2) {
            addCriterion("modle_code between", value1, value2, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleCodeNotBetween(String value1, String value2) {
            addCriterion("modle_code not between", value1, value2, "modleCode");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlIsNull() {
            addCriterion("modle_img_url is null");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlIsNotNull() {
            addCriterion("modle_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlEqualTo(String value) {
            addCriterion("modle_img_url =", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlNotEqualTo(String value) {
            addCriterion("modle_img_url <>", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlGreaterThan(String value) {
            addCriterion("modle_img_url >", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("modle_img_url >=", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlLessThan(String value) {
            addCriterion("modle_img_url <", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlLessThanOrEqualTo(String value) {
            addCriterion("modle_img_url <=", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlLike(String value) {
            addCriterion("modle_img_url like", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlNotLike(String value) {
            addCriterion("modle_img_url not like", value, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlIn(List<String> values) {
            addCriterion("modle_img_url in", values, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlNotIn(List<String> values) {
            addCriterion("modle_img_url not in", values, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlBetween(String value1, String value2) {
            addCriterion("modle_img_url between", value1, value2, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleImgUrlNotBetween(String value1, String value2) {
            addCriterion("modle_img_url not between", value1, value2, "modleImgUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlIsNull() {
            addCriterion("modle_in_url is null");
            return (Criteria) this;
        }

        public Criteria andModleInUrlIsNotNull() {
            addCriterion("modle_in_url is not null");
            return (Criteria) this;
        }

        public Criteria andModleInUrlEqualTo(String value) {
            addCriterion("modle_in_url =", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlNotEqualTo(String value) {
            addCriterion("modle_in_url <>", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlGreaterThan(String value) {
            addCriterion("modle_in_url >", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlGreaterThanOrEqualTo(String value) {
            addCriterion("modle_in_url >=", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlLessThan(String value) {
            addCriterion("modle_in_url <", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlLessThanOrEqualTo(String value) {
            addCriterion("modle_in_url <=", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlLike(String value) {
            addCriterion("modle_in_url like", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlNotLike(String value) {
            addCriterion("modle_in_url not like", value, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlIn(List<String> values) {
            addCriterion("modle_in_url in", values, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlNotIn(List<String> values) {
            addCriterion("modle_in_url not in", values, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlBetween(String value1, String value2) {
            addCriterion("modle_in_url between", value1, value2, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleInUrlNotBetween(String value1, String value2) {
            addCriterion("modle_in_url not between", value1, value2, "modleInUrl");
            return (Criteria) this;
        }

        public Criteria andModleIsadminIsNull() {
            addCriterion("modle_isadmin is null");
            return (Criteria) this;
        }

        public Criteria andModleIsadminIsNotNull() {
            addCriterion("modle_isadmin is not null");
            return (Criteria) this;
        }

        public Criteria andModleIsadminEqualTo(Boolean value) {
            addCriterion("modle_isadmin =", value, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminNotEqualTo(Boolean value) {
            addCriterion("modle_isadmin <>", value, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminGreaterThan(Boolean value) {
            addCriterion("modle_isadmin >", value, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminGreaterThanOrEqualTo(Boolean value) {
            addCriterion("modle_isadmin >=", value, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminLessThan(Boolean value) {
            addCriterion("modle_isadmin <", value, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminLessThanOrEqualTo(Boolean value) {
            addCriterion("modle_isadmin <=", value, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminIn(List<Boolean> values) {
            addCriterion("modle_isadmin in", values, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminNotIn(List<Boolean> values) {
            addCriterion("modle_isadmin not in", values, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminBetween(Boolean value1, Boolean value2) {
            addCriterion("modle_isadmin between", value1, value2, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleIsadminNotBetween(Boolean value1, Boolean value2) {
            addCriterion("modle_isadmin not between", value1, value2, "modleIsadmin");
            return (Criteria) this;
        }

        public Criteria andModleOrderIsNull() {
            addCriterion("modle_order is null");
            return (Criteria) this;
        }

        public Criteria andModleOrderIsNotNull() {
            addCriterion("modle_order is not null");
            return (Criteria) this;
        }

        public Criteria andModleOrderEqualTo(Integer value) {
            addCriterion("modle_order =", value, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderNotEqualTo(Integer value) {
            addCriterion("modle_order <>", value, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderGreaterThan(Integer value) {
            addCriterion("modle_order >", value, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("modle_order >=", value, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderLessThan(Integer value) {
            addCriterion("modle_order <", value, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderLessThanOrEqualTo(Integer value) {
            addCriterion("modle_order <=", value, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderIn(List<Integer> values) {
            addCriterion("modle_order in", values, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderNotIn(List<Integer> values) {
            addCriterion("modle_order not in", values, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderBetween(Integer value1, Integer value2) {
            addCriterion("modle_order between", value1, value2, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModleOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("modle_order not between", value1, value2, "modleOrder");
            return (Criteria) this;
        }

        public Criteria andModelStateIsNull() {
            addCriterion("model_state is null");
            return (Criteria) this;
        }

        public Criteria andModelStateIsNotNull() {
            addCriterion("model_state is not null");
            return (Criteria) this;
        }

        public Criteria andModelStateEqualTo(String value) {
            addCriterion("model_state =", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateNotEqualTo(String value) {
            addCriterion("model_state <>", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateGreaterThan(String value) {
            addCriterion("model_state >", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateGreaterThanOrEqualTo(String value) {
            addCriterion("model_state >=", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateLessThan(String value) {
            addCriterion("model_state <", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateLessThanOrEqualTo(String value) {
            addCriterion("model_state <=", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateLike(String value) {
            addCriterion("model_state like", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateNotLike(String value) {
            addCriterion("model_state not like", value, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateIn(List<String> values) {
            addCriterion("model_state in", values, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateNotIn(List<String> values) {
            addCriterion("model_state not in", values, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateBetween(String value1, String value2) {
            addCriterion("model_state between", value1, value2, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelStateNotBetween(String value1, String value2) {
            addCriterion("model_state not between", value1, value2, "modelState");
            return (Criteria) this;
        }

        public Criteria andModelCreateIsNull() {
            addCriterion("model_create is null");
            return (Criteria) this;
        }

        public Criteria andModelCreateIsNotNull() {
            addCriterion("model_create is not null");
            return (Criteria) this;
        }

        public Criteria andModelCreateEqualTo(Date value) {
            addCriterionForJDBCTime("model_create =", value, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateNotEqualTo(Date value) {
            addCriterionForJDBCTime("model_create <>", value, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateGreaterThan(Date value) {
            addCriterionForJDBCTime("model_create >", value, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("model_create >=", value, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateLessThan(Date value) {
            addCriterionForJDBCTime("model_create <", value, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("model_create <=", value, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateIn(List<Date> values) {
            addCriterionForJDBCTime("model_create in", values, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateNotIn(List<Date> values) {
            addCriterionForJDBCTime("model_create not in", values, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("model_create between", value1, value2, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelCreateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("model_create not between", value1, value2, "modelCreate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateIsNull() {
            addCriterion("model_update is null");
            return (Criteria) this;
        }

        public Criteria andModelUpdateIsNotNull() {
            addCriterion("model_update is not null");
            return (Criteria) this;
        }

        public Criteria andModelUpdateEqualTo(Date value) {
            addCriterionForJDBCTime("model_update =", value, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateNotEqualTo(Date value) {
            addCriterionForJDBCTime("model_update <>", value, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateGreaterThan(Date value) {
            addCriterionForJDBCTime("model_update >", value, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("model_update >=", value, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateLessThan(Date value) {
            addCriterionForJDBCTime("model_update <", value, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("model_update <=", value, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateIn(List<Date> values) {
            addCriterionForJDBCTime("model_update in", values, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateNotIn(List<Date> values) {
            addCriterionForJDBCTime("model_update not in", values, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("model_update between", value1, value2, "modelUpdate");
            return (Criteria) this;
        }

        public Criteria andModelUpdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("model_update not between", value1, value2, "modelUpdate");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}