package com.hbxy.tg.portal.admin.org.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

/**
 * user_login
 * @author 
 */
public class UserLogin implements Serializable {
    private Integer userId;

    private String userUser;

    private String userPass;

    private Integer userPassTime;

    private String userName;

    private String userEmail;

    private String userTel;

    private String userIssysacc;

    private String userIsable;

    private Boolean userIsunlogin;

    private String userLoginAddress;

    private String userLoginTime;

    @Override
    public String toString() {
        return "UserLogin{" +
                "userId=" + userId +
                ", userUser='" + userUser + '\'' +
                ", userPass='" + userPass + '\'' +
                ", userPassTime=" + userPassTime +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userTel='" + userTel + '\'' +
                ", userIssysacc=" + userIssysacc +
                ", userIsable=" + userIsable +
                ", userIsunlogin=" + userIsunlogin +
                ", userLoginAddress='" + userLoginAddress + '\'' +
                ", userLoginTime='" + userLoginTime + '\'' +
                ", userBanTime=" + userBanTime +
                ", userBanName='" + userBanName + '\'' +
                ", userFailTime=" + userFailTime +
                ", userState='" + userState + '\'' +
                ", userCreate=" + userCreate +
                ", userUpdate=" + userUpdate +
                ", userPassNeedEdit=" + Arrays.toString(userPassNeedEdit) +
                '}';
    }

    private Date userBanTime;

    private String userBanName;

    private Integer userFailTime;

    private String userState;

    private Date userCreate;

    private Date userUpdate;

    private byte[] userPassNeedEdit;

    private static final long serialVersionUID = 1L;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserUser() {
        return userUser;
    }

    public void setUserUser(String userUser) {
        this.userUser = userUser;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public Integer getUserPassTime() {
        return userPassTime;
    }

    public void setUserPassTime(Integer userPassTime) {
        this.userPassTime = userPassTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserTel() {
        return userTel;
    }

    public void setUserTel(String userTel) {
        this.userTel = userTel;
    }

    public String getUserIssysacc() {
        return userIssysacc;
    }

    public void setUserIssysacc(String userIssysacc) {
        this.userIssysacc = userIssysacc;
    }

    public String getUserIsable() {
        return userIsable;
    }

    public void setUserIsable(String userIsable) {
        this.userIsable = userIsable;
    }

    public Boolean getUserIsunlogin() {
        return userIsunlogin;
    }

    public void setUserIsunlogin(Boolean userIsunlogin) {
        this.userIsunlogin = userIsunlogin;
    }

    public String getUserLoginAddress() {
        return userLoginAddress;
    }

    public void setUserLoginAddress(String userLoginAddress) {
        this.userLoginAddress = userLoginAddress;
    }

    public String getUserLoginTime() {
        return userLoginTime;
    }

    public void setUserLoginTime(String userLoginTime) {
        this.userLoginTime = userLoginTime;
    }

    public Date getUserBanTime() {
        return userBanTime;
    }

    public void setUserBanTime(Date userBanTime) {
        this.userBanTime = userBanTime;
    }

    public String getUserBanName() {
        return userBanName;
    }

    public void setUserBanName(String userBanName) {
        this.userBanName = userBanName;
    }

    public Integer getUserFailTime() {
        return userFailTime;
    }

    public void setUserFailTime(Integer userFailTime) {
        this.userFailTime = userFailTime;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public Date getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(Date userCreate) {
        this.userCreate = userCreate;
    }

    public Date getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Date userUpdate) {
        this.userUpdate = userUpdate;
    }

    public byte[] getUserPassNeedEdit() {
        return userPassNeedEdit;
    }

    public void setUserPassNeedEdit(byte[] userPassNeedEdit) {
        this.userPassNeedEdit = userPassNeedEdit;
    }
}