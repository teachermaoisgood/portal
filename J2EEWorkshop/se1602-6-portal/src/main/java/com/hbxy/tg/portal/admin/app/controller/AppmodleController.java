package com.hbxy.tg.portal.admin.app.controller;

import com.hbxy.tg.portal.admin.app.model.Appmodle;
import com.hbxy.tg.portal.admin.app.service.AppmodleService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/modle")
public class AppmodleController {
    @Autowired
    AppmodleService appmodleService;
    @RequestMapping("/modletable")
    @ResponseBody
    public JSONObject apptable() {
        JSONObject json = new JSONObject();
        JSONObject jsondata = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1);
        List<Appmodle> list = appmodleService.selectAll();
        for (int i = 0; i < list.size(); i++) {
            jsondata.put("modleId", list.get(i).getModleId());
            jsondata.put("modleName", list.get(i).getModleName());
            jsondata.put("modelState", list.get(i).getModelState());
            jsondata.put("appId", list.get(i).getAppId());
            jsonArray.add(jsondata);
            json.put("data", jsonArray);
        }
        return json;
    }
        //    模块添加
        @RequestMapping( "/addmod")
        @ResponseBody
        public Integer modadd(@RequestBody Appmodle modle){
            System.out.println(modle.toString());
            appmodleService.insert(modle);
            return 1;
        }
        //    模块修改
        @RequestMapping("/modedit")
        @ResponseBody
        public Integer modedit(@RequestBody Appmodle modle){
            System.out.println(modle.toString());
            appmodleService.updateByPrimaryKeySelective(modle);
            return 1;
        }
        //    模块删除
        @RequestMapping("/moddel" )
        @ResponseBody
        public String moddel(@RequestBody Appmodle modle){
            appmodleService.deleteByPrimaryKey(modle.getModleId());
            return "200";
        }
}