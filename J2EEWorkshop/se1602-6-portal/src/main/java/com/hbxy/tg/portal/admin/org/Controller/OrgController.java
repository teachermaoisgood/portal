package com.hbxy.tg.portal.admin.org.Controller;

import com.hbxy.tg.portal.admin.org.Service.UserLoginService;
import com.hbxy.tg.portal.admin.org.Service.UserService;
import com.hbxy.tg.portal.admin.org.model.UserLogin;
import com.hbxy.tg.portal.admin.org.model.UserLoginExample;

import com.hbxy.tg.portal.admin.org.model.UserRole;
import com.hbxy.tg.portal.admin.org.model.UserRoleExample;
import com.hbxy.tg.portal.admin.right.model.*;
import com.hbxy.tg.portal.admin.right.model.Role;
import com.hbxy.tg.portal.admin.right.service.RoleService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrgController {
    @Autowired
    private UserLoginService userLoginService;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/usertable")
    @ResponseBody
    public JSONObject user_table() {
        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsondata = new JSONObject();
        json.put("code", 0);
        json.put("msg", "");
        json.put("count", 1000);
        UserLoginExample ule = new UserLoginExample();
        List<UserLogin> list = userLoginService.selectByExample(ule);

        UserRoleExample userRoleExample = new UserRoleExample();
        List<UserRole> list1 = userService.selectByExample(userRoleExample);
        RoleExample roleExample = new RoleExample();
        List<Role> list2 = roleService.selectByExample(roleExample);


        for (int i = 0; i < list.size(); i++) {
            List<UserLogin> ullist = userLoginService.selectByIssysacc(ule);
          //  List l=new ArrayList();
            jsondata.put("userId", list.get(i).getUserId());
            jsondata.put("userName", list.get(i).getUserName());
            jsondata.put("userEmail", list.get(i).getUserEmail());
            jsondata.put("userTel", list.get(i).getUserTel());
            //不表示管理员
//            String right="";
//           for (UserLogin ur:ullist){
//
//              right+=ur.getUserIssysacc()+" ";
//          }
            jsondata.put("userRole", list.get(i).getUserIssysacc());
            jsondata.put("abc", list.get(i).getUserIssysacc());
            //    jsondata.put("userIssysacc","普通用户");
            jsonArray.add(jsondata);
            json.put("data", jsonArray);
        }
        return json;

    }

    @RequestMapping("adduser")
    @ResponseBody
    public Integer adduser(@RequestBody UserLogin user) {
        System.out.println(user.toString());
        user.setUserIssysacc("1");
        userLoginService.insertSelective(user);
        return 1;
    }

    @RequestMapping("deluser")
    @ResponseBody
    public Integer deluser(@RequestBody UserLogin user) {
        System.out.println(user.toString());
        userLoginService.deleteByPrimaryKey(user.getUserId());
        return 1;
    }

    @RequestMapping("/edituser")
    @ResponseBody
    public Integer edituser(@RequestBody UserLogin user) {
        System.out.println(user.toString());
        userLoginService.updateByPrimaryKey(user);
        return 1;
    }

    @RequestMapping("addrole")
    @ResponseBody
    public Integer addrole(@RequestBody UserLogin user) {
        System.out.println((user.toString()));
        userLoginService.insertSelective(user);
        return 1;
    }

    @RequestMapping("/peo_role")
    @ResponseBody
    public String peo_role(String data[]) {
        System.out.println(data[0]);

        //获得该角色已经授权的权限
        for (String s : data) {
            System.out.println(s);

        }
        UserRole userRole=new UserRole();
        userRole.setUserD(Integer.valueOf(data[0]));

        RoleExample roleExample = new RoleExample();
//        RoleExample.Criteria roleCriteria = roleExample.createCriteria();
//        roleCriteria.andRoleIdEqualTo(userRole.getRoleId());

        List<Role> roles = roleService.selectByExample(roleExample);
        for(Role r:roles){
            System.out.println(String.valueOf("角色id："+r.getRoleId()+"角色现有权限："+r.getRoleName()));
            userService.deleteByPrimaryKey(r.getRoleId());
        }
        Role role = new Role();
        role.setRoleId(role.getRoleId());
        for (int i = 1; i < data.length; i++) {

            userRole.setRoleId(Integer.valueOf(data[i]));
            userService.insert(userRole);
        }


        return "添加成功";
    }
}