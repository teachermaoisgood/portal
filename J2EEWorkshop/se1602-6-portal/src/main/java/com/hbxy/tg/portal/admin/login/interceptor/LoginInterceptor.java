package com.hbxy.tg.portal.admin.login.interceptor;


import com.hbxy.tg.portal.admin.org.model.UserLogin;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginInterceptor implements HandlerInterceptor {

    private static final String[] IGNORE_URI = {"/login"};

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws IOException {
        boolean flag = false;
        String url = request.getServletPath();
        for (String s : IGNORE_URI) {
            if (url.contains(s)) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            UserLogin userLogin = (UserLogin) request.getSession().getAttribute("USER_SESSION");
            if (userLogin == null) {
                request.setAttribute("message", "请先登录");

                try {

                    response.sendRedirect(request.getContextPath() + "/login");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return flag;
            } else {
                flag = true;
                return flag;
            }

        }
        return flag;
    }
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
