package com.hbxy.tg.portal.admin.right.model;

import java.io.Serializable;
import java.util.Date;

/**
 * role
 * @author 
 */
public class Role_ {

    private String roleName;

    @Override
    public String toString() {
        return "Role_{" +
                "roleName='" + roleName + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", roleDes='" + roleDes + '\'' +
                '}';
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleDes() {
        return roleDes;
    }

    public void setRoleDes(String roleDes) {
        this.roleDes = roleDes;
    }

    private String roleCode;

    private String roleDes;


}