package com.hbxy.tg.portal.admin.app.mapper;

import com.hbxy.tg.portal.admin.app.model.ChildSys;
import com.hbxy.tg.portal.admin.app.model.ChildSysExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ChildSysDAO {
    long countByExample(ChildSysExample example);

    int deleteByExample(ChildSysExample example);

    int deleteByPrimaryKey(Integer appId);

    int insert(ChildSys record);

    int insertSelective(ChildSys record);

    List<ChildSys> selectByExample(ChildSysExample example);

    ChildSys selectByPrimaryKey(Integer appId);

    int updateByExampleSelective(@Param("record") ChildSys record, @Param("example") ChildSysExample example);

    int updateByExample(@Param("record") ChildSys record, @Param("example") ChildSysExample example);

    int updateByPrimaryKeySelective(ChildSys record);

    int updateByPrimaryKey(ChildSys record);

    List<ChildSys> selectAll();
}