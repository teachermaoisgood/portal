package com.hbxy.tg.portal.admin.org.model;

import java.io.Serializable;

/**
 * user_role
 *
 * @author
 */
public class UserRole implements Serializable {
    private Integer rolePartyId;

    private Integer roleId;

    private Integer userD;

    private static final long serialVersionUID = 1L;

    public Integer getRolePartyId() {
        return rolePartyId;
    }

    public void setRolePartyId(Integer rolePartyId) {
        this.rolePartyId = rolePartyId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getUserD() {
        return userD;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "rolePartyId=" + rolePartyId +
                ", roleId=" + roleId +
                ", userD=" + userD +
                '}';
    }

    public void setUserD(Integer userD) {
        this.userD = userD;
    }


}