package com.hbxy.tg.portal.admin.app.mapper;

import com.hbxy.tg.portal.admin.app.model.Appmodle;
import com.hbxy.tg.portal.admin.app.model.AppmodleExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface AppmodleDAO {
    long countByExample(AppmodleExample example);

    int deleteByExample(AppmodleExample example);

    int deleteByPrimaryKey(Integer modleId);

    int insert(Appmodle record);

    int insertSelective(Appmodle record);

    List<Appmodle> selectByExample(AppmodleExample example);

    Appmodle selectByPrimaryKey(Integer modleId);

    int updateByExampleSelective(@Param("record") Appmodle record, @Param("example") AppmodleExample example);

    int updateByExample(@Param("record") Appmodle record, @Param("example") AppmodleExample example);

    int updateByPrimaryKeySelective(Appmodle record);

    int updateByPrimaryKey(Appmodle record);

    List<Appmodle> selectAll();
}