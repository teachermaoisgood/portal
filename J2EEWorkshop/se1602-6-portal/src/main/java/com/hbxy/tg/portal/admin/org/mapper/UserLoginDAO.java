package com.hbxy.tg.portal.admin.org.mapper;

import com.hbxy.tg.portal.admin.org.model.UserLogin;
import com.hbxy.tg.portal.admin.org.model.UserLoginExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface UserLoginDAO {
    long countByExample(UserLoginExample example);

    int deleteByExample(UserLoginExample example);

    int deleteByPrimaryKey(Integer userId);

    int insert(UserLogin record);

    int insertSelective(UserLogin record);

    List<UserLogin> selectByExampleWithBLOBs(UserLoginExample example);

    List<UserLogin> selectByExazzmple(UserLoginExample example);
    List<UserLogin> selectRoleAll(Integer userId);
    List<UserLogin> selectByIssysacc(UserLoginExample example);

    UserLogin selectByPrimaryKey(Integer userId);

    int updateByExampleSelective(@Param("record") UserLogin record, @Param("example") UserLoginExample example);

    int updateByExampleWithBLOBs(@Param("record") UserLogin record, @Param("example") UserLoginExample example);

    int updateByExample(@Param("record") UserLogin record, @Param("example") UserLoginExample example);

    int updateByPrimaryKeySelective(UserLogin record);

    int updateByPrimaryKeyWithBLOBs(UserLogin record);
    int updateByPrimaryKey(UserLogin record);

    List<UserLogin> selectByExample(UserLoginExample example);
}