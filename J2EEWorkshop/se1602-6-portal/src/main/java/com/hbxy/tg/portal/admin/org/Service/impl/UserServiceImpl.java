package com.hbxy.tg.portal.admin.org.Service.impl;

import com.hbxy.tg.portal.admin.org.Service.UserService;
import com.hbxy.tg.portal.admin.org.mapper.UserRoleDAO;
import com.hbxy.tg.portal.admin.org.model.UserRole;
import com.hbxy.tg.portal.admin.org.model.UserRoleExample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRoleDAO mapper;

    @Override
    public long countByExample(UserRoleExample example) {
        return mapper.countByExample(example);
    }

    @Override
    public int deleteByExample(UserRoleExample example) {
        return mapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer rolePartyId) {
        return mapper.deleteByPrimaryKey(rolePartyId);
    }

    @Override
    public int insert(UserRole record) {
        return mapper.insert(record);
    }

    @Override
    public int insertSelective(UserRole record) {
        return mapper.insertSelective(record);
    }

    @Override
    public List<UserRole> selectByExample(UserRoleExample example) {
        return mapper.selectByExample(example);
    }

    @Override
    public List<UserRole> selectAll() {
        return mapper.selectAll();
    }


    @Override
    public UserRole selectByPrimaryKey(Integer rolePartyId) {
        return mapper.selectByPrimaryKey(rolePartyId);
    }

    @Override
    public int updateByExampleSelective(UserRole record, UserRoleExample example) {
        return mapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(UserRole record, UserRoleExample example) {
        return mapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(UserRole record) {
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(UserRole record) {
        return mapper.updateByPrimaryKey(record);
    }
}

