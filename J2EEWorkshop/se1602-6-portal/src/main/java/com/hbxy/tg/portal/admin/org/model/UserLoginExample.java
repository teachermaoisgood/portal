package com.hbxy.tg.portal.admin.org.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class UserLoginExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public UserLoginExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserUserIsNull() {
            addCriterion("user_user is null");
            return (Criteria) this;
        }

        public Criteria andUserUserIsNotNull() {
            addCriterion("user_user is not null");
            return (Criteria) this;
        }

        public Criteria andUserUserEqualTo(String value) {
            addCriterion("user_user =", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserNotEqualTo(String value) {
            addCriterion("user_user <>", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserGreaterThan(String value) {
            addCriterion("user_user >", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserGreaterThanOrEqualTo(String value) {
            addCriterion("user_user >=", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserLessThan(String value) {
            addCriterion("user_user <", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserLessThanOrEqualTo(String value) {
            addCriterion("user_user <=", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserLike(String value) {
            addCriterion("user_user like", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserNotLike(String value) {
            addCriterion("user_user not like", value, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserIn(List<String> values) {
            addCriterion("user_user in", values, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserNotIn(List<String> values) {
            addCriterion("user_user not in", values, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserBetween(String value1, String value2) {
            addCriterion("user_user between", value1, value2, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserUserNotBetween(String value1, String value2) {
            addCriterion("user_user not between", value1, value2, "userUser");
            return (Criteria) this;
        }

        public Criteria andUserPassIsNull() {
            addCriterion("user_pass is null");
            return (Criteria) this;
        }

        public Criteria andUserPassIsNotNull() {
            addCriterion("user_pass is not null");
            return (Criteria) this;
        }

        public Criteria andUserPassEqualTo(String value) {
            addCriterion("user_pass =", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotEqualTo(String value) {
            addCriterion("user_pass <>", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassGreaterThan(String value) {
            addCriterion("user_pass >", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassGreaterThanOrEqualTo(String value) {
            addCriterion("user_pass >=", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassLessThan(String value) {
            addCriterion("user_pass <", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassLessThanOrEqualTo(String value) {
            addCriterion("user_pass <=", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassLike(String value) {
            addCriterion("user_pass like", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotLike(String value) {
            addCriterion("user_pass not like", value, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassIn(List<String> values) {
            addCriterion("user_pass in", values, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotIn(List<String> values) {
            addCriterion("user_pass not in", values, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassBetween(String value1, String value2) {
            addCriterion("user_pass between", value1, value2, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassNotBetween(String value1, String value2) {
            addCriterion("user_pass not between", value1, value2, "userPass");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeIsNull() {
            addCriterion("user_pass_time is null");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeIsNotNull() {
            addCriterion("user_pass_time is not null");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeEqualTo(Integer value) {
            addCriterion("user_pass_time =", value, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeNotEqualTo(Integer value) {
            addCriterion("user_pass_time <>", value, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeGreaterThan(Integer value) {
            addCriterion("user_pass_time >", value, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_pass_time >=", value, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeLessThan(Integer value) {
            addCriterion("user_pass_time <", value, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeLessThanOrEqualTo(Integer value) {
            addCriterion("user_pass_time <=", value, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeIn(List<Integer> values) {
            addCriterion("user_pass_time in", values, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeNotIn(List<Integer> values) {
            addCriterion("user_pass_time not in", values, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeBetween(Integer value1, Integer value2) {
            addCriterion("user_pass_time between", value1, value2, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserPassTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("user_pass_time not between", value1, value2, "userPassTime");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserEmailIsNull() {
            addCriterion("user_email is null");
            return (Criteria) this;
        }

        public Criteria andUserEmailIsNotNull() {
            addCriterion("user_email is not null");
            return (Criteria) this;
        }

        public Criteria andUserEmailEqualTo(String value) {
            addCriterion("user_email =", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotEqualTo(String value) {
            addCriterion("user_email <>", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailGreaterThan(String value) {
            addCriterion("user_email >", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailGreaterThanOrEqualTo(String value) {
            addCriterion("user_email >=", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailLessThan(String value) {
            addCriterion("user_email <", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailLessThanOrEqualTo(String value) {
            addCriterion("user_email <=", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailLike(String value) {
            addCriterion("user_email like", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotLike(String value) {
            addCriterion("user_email not like", value, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailIn(List<String> values) {
            addCriterion("user_email in", values, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotIn(List<String> values) {
            addCriterion("user_email not in", values, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailBetween(String value1, String value2) {
            addCriterion("user_email between", value1, value2, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserEmailNotBetween(String value1, String value2) {
            addCriterion("user_email not between", value1, value2, "userEmail");
            return (Criteria) this;
        }

        public Criteria andUserTelIsNull() {
            addCriterion("user_tel is null");
            return (Criteria) this;
        }

        public Criteria andUserTelIsNotNull() {
            addCriterion("user_tel is not null");
            return (Criteria) this;
        }

        public Criteria andUserTelEqualTo(String value) {
            addCriterion("user_tel =", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotEqualTo(String value) {
            addCriterion("user_tel <>", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelGreaterThan(String value) {
            addCriterion("user_tel >", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelGreaterThanOrEqualTo(String value) {
            addCriterion("user_tel >=", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelLessThan(String value) {
            addCriterion("user_tel <", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelLessThanOrEqualTo(String value) {
            addCriterion("user_tel <=", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelLike(String value) {
            addCriterion("user_tel like", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotLike(String value) {
            addCriterion("user_tel not like", value, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelIn(List<String> values) {
            addCriterion("user_tel in", values, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotIn(List<String> values) {
            addCriterion("user_tel not in", values, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelBetween(String value1, String value2) {
            addCriterion("user_tel between", value1, value2, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserTelNotBetween(String value1, String value2) {
            addCriterion("user_tel not between", value1, value2, "userTel");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccIsNull() {
            addCriterion("user_issysacc is null");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccIsNotNull() {
            addCriterion("user_issysacc is not null");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccEqualTo(Boolean value) {
            addCriterion("user_issysacc =", value, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccNotEqualTo(Boolean value) {
            addCriterion("user_issysacc <>", value, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccGreaterThan(Boolean value) {
            addCriterion("user_issysacc >", value, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccGreaterThanOrEqualTo(Boolean value) {
            addCriterion("user_issysacc >=", value, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccLessThan(Boolean value) {
            addCriterion("user_issysacc <", value, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccLessThanOrEqualTo(Boolean value) {
            addCriterion("user_issysacc <=", value, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccIn(List<Boolean> values) {
            addCriterion("user_issysacc in", values, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccNotIn(List<Boolean> values) {
            addCriterion("user_issysacc not in", values, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccBetween(Boolean value1, Boolean value2) {
            addCriterion("user_issysacc between", value1, value2, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIssysaccNotBetween(Boolean value1, Boolean value2) {
            addCriterion("user_issysacc not between", value1, value2, "userIssysacc");
            return (Criteria) this;
        }

        public Criteria andUserIsableIsNull() {
            addCriterion("user_isable is null");
            return (Criteria) this;
        }

        public Criteria andUserIsableIsNotNull() {
            addCriterion("user_isable is not null");
            return (Criteria) this;
        }

        public Criteria andUserIsableEqualTo(Boolean value) {
            addCriterion("user_isable =", value, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableNotEqualTo(Boolean value) {
            addCriterion("user_isable <>", value, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableGreaterThan(Boolean value) {
            addCriterion("user_isable >", value, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableGreaterThanOrEqualTo(Boolean value) {
            addCriterion("user_isable >=", value, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableLessThan(Boolean value) {
            addCriterion("user_isable <", value, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableLessThanOrEqualTo(Boolean value) {
            addCriterion("user_isable <=", value, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableIn(List<Boolean> values) {
            addCriterion("user_isable in", values, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableNotIn(List<Boolean> values) {
            addCriterion("user_isable not in", values, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableBetween(Boolean value1, Boolean value2) {
            addCriterion("user_isable between", value1, value2, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsableNotBetween(Boolean value1, Boolean value2) {
            addCriterion("user_isable not between", value1, value2, "userIsable");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginIsNull() {
            addCriterion("user_isunlogin is null");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginIsNotNull() {
            addCriterion("user_isunlogin is not null");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginEqualTo(Boolean value) {
            addCriterion("user_isunlogin =", value, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginNotEqualTo(Boolean value) {
            addCriterion("user_isunlogin <>", value, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginGreaterThan(Boolean value) {
            addCriterion("user_isunlogin >", value, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginGreaterThanOrEqualTo(Boolean value) {
            addCriterion("user_isunlogin >=", value, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginLessThan(Boolean value) {
            addCriterion("user_isunlogin <", value, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginLessThanOrEqualTo(Boolean value) {
            addCriterion("user_isunlogin <=", value, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginIn(List<Boolean> values) {
            addCriterion("user_isunlogin in", values, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginNotIn(List<Boolean> values) {
            addCriterion("user_isunlogin not in", values, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginBetween(Boolean value1, Boolean value2) {
            addCriterion("user_isunlogin between", value1, value2, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserIsunloginNotBetween(Boolean value1, Boolean value2) {
            addCriterion("user_isunlogin not between", value1, value2, "userIsunlogin");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressIsNull() {
            addCriterion("user_login_address is null");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressIsNotNull() {
            addCriterion("user_login_address is not null");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressEqualTo(String value) {
            addCriterion("user_login_address =", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressNotEqualTo(String value) {
            addCriterion("user_login_address <>", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressGreaterThan(String value) {
            addCriterion("user_login_address >", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressGreaterThanOrEqualTo(String value) {
            addCriterion("user_login_address >=", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressLessThan(String value) {
            addCriterion("user_login_address <", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressLessThanOrEqualTo(String value) {
            addCriterion("user_login_address <=", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressLike(String value) {
            addCriterion("user_login_address like", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressNotLike(String value) {
            addCriterion("user_login_address not like", value, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressIn(List<String> values) {
            addCriterion("user_login_address in", values, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressNotIn(List<String> values) {
            addCriterion("user_login_address not in", values, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressBetween(String value1, String value2) {
            addCriterion("user_login_address between", value1, value2, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginAddressNotBetween(String value1, String value2) {
            addCriterion("user_login_address not between", value1, value2, "userLoginAddress");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeIsNull() {
            addCriterion("user_login_time is null");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeIsNotNull() {
            addCriterion("user_login_time is not null");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeEqualTo(String value) {
            addCriterion("user_login_time =", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeNotEqualTo(String value) {
            addCriterion("user_login_time <>", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeGreaterThan(String value) {
            addCriterion("user_login_time >", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeGreaterThanOrEqualTo(String value) {
            addCriterion("user_login_time >=", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeLessThan(String value) {
            addCriterion("user_login_time <", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeLessThanOrEqualTo(String value) {
            addCriterion("user_login_time <=", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeLike(String value) {
            addCriterion("user_login_time like", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeNotLike(String value) {
            addCriterion("user_login_time not like", value, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeIn(List<String> values) {
            addCriterion("user_login_time in", values, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeNotIn(List<String> values) {
            addCriterion("user_login_time not in", values, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeBetween(String value1, String value2) {
            addCriterion("user_login_time between", value1, value2, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserLoginTimeNotBetween(String value1, String value2) {
            addCriterion("user_login_time not between", value1, value2, "userLoginTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeIsNull() {
            addCriterion("user_ban_time is null");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeIsNotNull() {
            addCriterion("user_ban_time is not null");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeEqualTo(Date value) {
            addCriterionForJDBCTime("user_ban_time =", value, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeNotEqualTo(Date value) {
            addCriterionForJDBCTime("user_ban_time <>", value, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeGreaterThan(Date value) {
            addCriterionForJDBCTime("user_ban_time >", value, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("user_ban_time >=", value, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeLessThan(Date value) {
            addCriterionForJDBCTime("user_ban_time <", value, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("user_ban_time <=", value, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeIn(List<Date> values) {
            addCriterionForJDBCTime("user_ban_time in", values, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeNotIn(List<Date> values) {
            addCriterionForJDBCTime("user_ban_time not in", values, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("user_ban_time between", value1, value2, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanTimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("user_ban_time not between", value1, value2, "userBanTime");
            return (Criteria) this;
        }

        public Criteria andUserBanNameIsNull() {
            addCriterion("user_ban_name is null");
            return (Criteria) this;
        }

        public Criteria andUserBanNameIsNotNull() {
            addCriterion("user_ban_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserBanNameEqualTo(String value) {
            addCriterion("user_ban_name =", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameNotEqualTo(String value) {
            addCriterion("user_ban_name <>", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameGreaterThan(String value) {
            addCriterion("user_ban_name >", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_ban_name >=", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameLessThan(String value) {
            addCriterion("user_ban_name <", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameLessThanOrEqualTo(String value) {
            addCriterion("user_ban_name <=", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameLike(String value) {
            addCriterion("user_ban_name like", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameNotLike(String value) {
            addCriterion("user_ban_name not like", value, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameIn(List<String> values) {
            addCriterion("user_ban_name in", values, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameNotIn(List<String> values) {
            addCriterion("user_ban_name not in", values, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameBetween(String value1, String value2) {
            addCriterion("user_ban_name between", value1, value2, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserBanNameNotBetween(String value1, String value2) {
            addCriterion("user_ban_name not between", value1, value2, "userBanName");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeIsNull() {
            addCriterion("user_fail_time is null");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeIsNotNull() {
            addCriterion("user_fail_time is not null");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeEqualTo(Integer value) {
            addCriterion("user_fail_time =", value, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeNotEqualTo(Integer value) {
            addCriterion("user_fail_time <>", value, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeGreaterThan(Integer value) {
            addCriterion("user_fail_time >", value, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_fail_time >=", value, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeLessThan(Integer value) {
            addCriterion("user_fail_time <", value, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeLessThanOrEqualTo(Integer value) {
            addCriterion("user_fail_time <=", value, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeIn(List<Integer> values) {
            addCriterion("user_fail_time in", values, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeNotIn(List<Integer> values) {
            addCriterion("user_fail_time not in", values, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeBetween(Integer value1, Integer value2) {
            addCriterion("user_fail_time between", value1, value2, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserFailTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("user_fail_time not between", value1, value2, "userFailTime");
            return (Criteria) this;
        }

        public Criteria andUserStateIsNull() {
            addCriterion("user_state is null");
            return (Criteria) this;
        }

        public Criteria andUserStateIsNotNull() {
            addCriterion("user_state is not null");
            return (Criteria) this;
        }

        public Criteria andUserStateEqualTo(String value) {
            addCriterion("user_state =", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateNotEqualTo(String value) {
            addCriterion("user_state <>", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateGreaterThan(String value) {
            addCriterion("user_state >", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateGreaterThanOrEqualTo(String value) {
            addCriterion("user_state >=", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateLessThan(String value) {
            addCriterion("user_state <", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateLessThanOrEqualTo(String value) {
            addCriterion("user_state <=", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateLike(String value) {
            addCriterion("user_state like", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateNotLike(String value) {
            addCriterion("user_state not like", value, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateIn(List<String> values) {
            addCriterion("user_state in", values, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateNotIn(List<String> values) {
            addCriterion("user_state not in", values, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateBetween(String value1, String value2) {
            addCriterion("user_state between", value1, value2, "userState");
            return (Criteria) this;
        }

        public Criteria andUserStateNotBetween(String value1, String value2) {
            addCriterion("user_state not between", value1, value2, "userState");
            return (Criteria) this;
        }

        public Criteria andUserCreateIsNull() {
            addCriterion("user_create is null");
            return (Criteria) this;
        }

        public Criteria andUserCreateIsNotNull() {
            addCriterion("user_create is not null");
            return (Criteria) this;
        }

        public Criteria andUserCreateEqualTo(Date value) {
            addCriterionForJDBCTime("user_create =", value, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateNotEqualTo(Date value) {
            addCriterionForJDBCTime("user_create <>", value, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateGreaterThan(Date value) {
            addCriterionForJDBCTime("user_create >", value, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("user_create >=", value, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateLessThan(Date value) {
            addCriterionForJDBCTime("user_create <", value, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("user_create <=", value, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateIn(List<Date> values) {
            addCriterionForJDBCTime("user_create in", values, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateNotIn(List<Date> values) {
            addCriterionForJDBCTime("user_create not in", values, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("user_create between", value1, value2, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserCreateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("user_create not between", value1, value2, "userCreate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateIsNull() {
            addCriterion("user_update is null");
            return (Criteria) this;
        }

        public Criteria andUserUpdateIsNotNull() {
            addCriterion("user_update is not null");
            return (Criteria) this;
        }

        public Criteria andUserUpdateEqualTo(Date value) {
            addCriterionForJDBCTime("user_update =", value, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateNotEqualTo(Date value) {
            addCriterionForJDBCTime("user_update <>", value, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateGreaterThan(Date value) {
            addCriterionForJDBCTime("user_update >", value, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("user_update >=", value, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateLessThan(Date value) {
            addCriterionForJDBCTime("user_update <", value, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("user_update <=", value, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateIn(List<Date> values) {
            addCriterionForJDBCTime("user_update in", values, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateNotIn(List<Date> values) {
            addCriterionForJDBCTime("user_update not in", values, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("user_update between", value1, value2, "userUpdate");
            return (Criteria) this;
        }

        public Criteria andUserUpdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("user_update not between", value1, value2, "userUpdate");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}