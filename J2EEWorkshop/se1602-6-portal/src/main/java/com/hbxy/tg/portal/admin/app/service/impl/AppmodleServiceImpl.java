package com.hbxy.tg.portal.admin.app.service.impl;

import com.hbxy.tg.portal.admin.app.mapper.AppmodleDAO;
import com.hbxy.tg.portal.admin.app.model.Appmodle;
import com.hbxy.tg.portal.admin.app.model.AppmodleExample;
import com.hbxy.tg.portal.admin.app.service.AppmodleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppmodleServiceImpl implements AppmodleService {
    @Autowired
    private AppmodleDAO appmodleDAO;
    @Override
    public long countByExample(AppmodleExample example) {
        return 0;
    }

    @Override
    public int deleteByExample(AppmodleExample example) {
        return 0;
    }

    @Override
    public int deleteByPrimaryKey(Integer modeId) { return appmodleDAO.deleteByPrimaryKey(modeId);
    }

    @Override
    public int insert(Appmodle record) {
        return appmodleDAO.insert(record);
    }

    @Override
    public int insertSelective(Appmodle record) {
        return appmodleDAO.insertSelective(record);
    }

    @Override
    public List<Appmodle> selectByExample(AppmodleExample example) {
        return appmodleDAO.selectByExample(example);
    }

    @Override
    public List<Appmodle> selectAll() {
        return appmodleDAO.selectAll();
    }

    @Override
    public Appmodle selectByPrimaryKey(Integer modeId) {
        return appmodleDAO.selectByPrimaryKey(modeId);
    }

    @Override
    public int updateByExampleSelective(Appmodle record, AppmodleExample example) {
        return appmodleDAO.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Appmodle record, AppmodleExample example) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeySelective(Appmodle record) {
        return appmodleDAO.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Appmodle record) {
        return 0;
    }
}
