package com.hbxy.tg.portal.admin.app.service.impl;

import com.hbxy.tg.portal.admin.app.mapper.ChildSysDAO;
import com.hbxy.tg.portal.admin.app.model.ChildSys;
import com.hbxy.tg.portal.admin.app.model.ChildSysExample;

import com.hbxy.tg.portal.admin.app.service.ChildSysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChildSysServiceImpl implements ChildSysService {
    @Autowired
    private ChildSysDAO childSysDAO;
    @Override
    public long countByExample(ChildSysExample example) {
        return childSysDAO.countByExample(example);
    }

    @Override
    public int deleteByExample(ChildSysExample example) {
        return childSysDAO.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer appId) { return childSysDAO.deleteByPrimaryKey(appId);
    }

    @Override
    public int insert(ChildSys record) {
        return childSysDAO.insert(record);
    }

    @Override
    public int insertSelective(ChildSys record) {
        return childSysDAO.insertSelective(record);
    }

    @Override
    public List<ChildSys> selectByExample(ChildSysExample example) {
        return childSysDAO.selectByExample(example);
    }

    @Override
    public List<ChildSys> selectAll() {
        return childSysDAO.selectAll();
    }

    @Override
    public ChildSys selectByPrimaryKey(Integer appId) {
        return childSysDAO.selectByPrimaryKey(appId);
    }

    @Override
    public int updateByExampleSelective(ChildSys record, ChildSysExample example) {
        return childSysDAO.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(ChildSys record, ChildSysExample example) {
        return childSysDAO.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(ChildSys record) {
        return childSysDAO.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ChildSys record) {
        return childSysDAO.updateByPrimaryKey(record);
    }
}

