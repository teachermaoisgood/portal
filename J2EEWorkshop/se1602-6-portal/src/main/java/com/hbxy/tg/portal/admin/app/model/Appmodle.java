package com.hbxy.tg.portal.admin.app.model;

import java.io.Serializable;
import java.util.Date;

/**
 * appmodle
 * @author 
 */
public class Appmodle implements Serializable {
    private Integer modleId;

    private Integer appId;

    private String modleName;

    private String modleCode;

    private String modleImgUrl;

    @Override
    public String toString() {
        return "Appmodle{" +
                "modleId=" + modleId +
                ", appId=" + appId +
                ", modleName='" + modleName + '\'' +
                ", modleCode='" + modleCode + '\'' +
                ", modleImgUrl='" + modleImgUrl + '\'' +
                ", modleInUrl='" + modleInUrl + '\'' +
                ", modleIsadmin=" + modleIsadmin +
                ", modleOrder=" + modleOrder +
                ", modelState='" + modelState + '\'' +
                ", modelCreate=" + modelCreate +
                ", modelUpdate=" + modelUpdate +
                '}';
    }

    private String modleInUrl;

    private String modleIsadmin;

    private Integer modleOrder;

    private String modelState;

    private Date modelCreate;

    private Date modelUpdate;

    private static final long serialVersionUID = 1L;

    public Integer getModleId() {
        return modleId;
    }

    public void setModleId(Integer modleId) {
        this.modleId = modleId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getModleName() {
        return modleName;
    }

    public void setModleName(String modleName) {
        this.modleName = modleName;
    }

    public String getModleCode() {
        return modleCode;
    }

    public void setModleCode(String modleCode) {
        this.modleCode = modleCode;
    }

    public String getModleImgUrl() {
        return modleImgUrl;
    }

    public void setModleImgUrl(String modleImgUrl) {
        this.modleImgUrl = modleImgUrl;
    }

    public String getModleInUrl() {
        return modleInUrl;
    }

    public void setModleInUrl(String modleInUrl) {
        this.modleInUrl = modleInUrl;
    }

    public String getModleIsadmin() {
        return modleIsadmin;
    }

    public void setModleIsadmin(String modleIsadmin) {
        this.modleIsadmin = modleIsadmin;
    }

    public Integer getModleOrder() {
        return modleOrder;
    }

    public void setModleOrder(Integer modleOrder) {
        this.modleOrder = modleOrder;
    }

    public String getModelState() {
        return modelState;
    }

    public void setModelState(String modelState) {
        this.modelState = modelState;
    }

    public Date getModelCreate() {
        return modelCreate;
    }

    public void setModelCreate(Date modelCreate) {
        this.modelCreate = modelCreate;
    }

    public Date getModelUpdate() {
        return modelUpdate;
    }

    public void setModelUpdate(Date modelUpdate) {
        this.modelUpdate = modelUpdate;
    }
}