package com.hbxy.tg.portal.admin.right.model;

import java.io.Serializable;
import java.util.Date;

/**
 * role
 * @author 
 */
public class Role implements Serializable {
    private Integer roleId;

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", roleDes='" + roleDes + '\'' +
                ", roleOrder=" + roleOrder +
                ", roleState='" + roleState + '\'' +
                ", roleCreate=" + roleCreate +
                ", roleUpdate=" + roleUpdate +
                '}';
    }

    private String roleName;

    private String roleCode;

    private String roleDes;

    private Integer roleOrder;

    private String roleState;

    private Date roleCreate;

    private Date roleUpdate;

    private static final long serialVersionUID = 1L;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleDes() {
        return roleDes;
    }

    public void setRoleDes(String roleDes) {
        this.roleDes = roleDes;
    }

    public Integer getRoleOrder() {
        return roleOrder;
    }

    public void setRoleOrder(Integer roleOrder) {
        this.roleOrder = roleOrder;
    }

    public String getRoleState() {
        return roleState;
    }

    public void setRoleState(String roleState) {
        this.roleState = roleState;
    }

    public Date getRoleCreate() {
        return roleCreate;
    }

    public void setRoleCreate(Date roleCreate) {
        this.roleCreate = roleCreate;
    }

    public Date getRoleUpdate() {
        return roleUpdate;
    }

    public void setRoleUpdate(Date roleUpdate) {
        this.roleUpdate = roleUpdate;
    }
}