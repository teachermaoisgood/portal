package com.hbxy.tg.portal.admin.org.Service;


import com.hbxy.tg.portal.admin.org.model.UserRole;
import com.hbxy.tg.portal.admin.org.model.UserRoleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    long countByExample(UserRoleExample example);


    int deleteByExample(UserRoleExample example);

    int deleteByPrimaryKey(Integer rolePartyId);


    int insert(UserRole record);

    int insertSelective(UserRole record);

    List<UserRole> selectByExample(UserRoleExample example);

    List<UserRole> selectAll();



    UserRole selectByPrimaryKey(Integer rolePartyId);

    int updateByExampleSelective(@Param("record") UserRole record, @Param("example") UserRoleExample example);

    int updateByExample(@Param("record") UserRole record, @Param("example") UserRoleExample example);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);
}
