package com.hbxy.tg.portal.admin.app.service;

import com.hbxy.tg.portal.admin.app.model.ChildSys;
import com.hbxy.tg.portal.admin.app.model.ChildSysExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ChildSysService {
    long countByExample(ChildSysExample example);

    int deleteByExample(ChildSysExample example);

    int deleteByPrimaryKey(Integer appId);

    int insert(ChildSys record);

    int insertSelective(ChildSys record);

    List<ChildSys> selectByExample(ChildSysExample example);

    List<ChildSys> selectAll();

    ChildSys selectByPrimaryKey(Integer appId);

    int updateByExampleSelective(@Param("record") ChildSys record, @Param("example") ChildSysExample example);

    int updateByExample(@Param("record") ChildSys record, @Param("example") ChildSysExample example);

    int updateByPrimaryKeySelective(ChildSys record);

    int updateByPrimaryKey(ChildSys record);
}