package com.hbxy.tg.portal.admin.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ChildSysExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public ChildSysExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andAppIdIsNull() {
            addCriterion("app_id is null");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNotNull() {
            addCriterion("app_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdEqualTo(Integer value) {
            addCriterion("app_id =", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotEqualTo(Integer value) {
            addCriterion("app_id <>", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThan(Integer value) {
            addCriterion("app_id >", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("app_id >=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThan(Integer value) {
            addCriterion("app_id <", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThanOrEqualTo(Integer value) {
            addCriterion("app_id <=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdIn(List<Integer> values) {
            addCriterion("app_id in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotIn(List<Integer> values) {
            addCriterion("app_id not in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdBetween(Integer value1, Integer value2) {
            addCriterion("app_id between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotBetween(Integer value1, Integer value2) {
            addCriterion("app_id not between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNull() {
            addCriterion("app_name is null");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNotNull() {
            addCriterion("app_name is not null");
            return (Criteria) this;
        }

        public Criteria andAppNameEqualTo(String value) {
            addCriterion("app_name =", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotEqualTo(String value) {
            addCriterion("app_name <>", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThan(String value) {
            addCriterion("app_name >", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThanOrEqualTo(String value) {
            addCriterion("app_name >=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThan(String value) {
            addCriterion("app_name <", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThanOrEqualTo(String value) {
            addCriterion("app_name <=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLike(String value) {
            addCriterion("app_name like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotLike(String value) {
            addCriterion("app_name not like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameIn(List<String> values) {
            addCriterion("app_name in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotIn(List<String> values) {
            addCriterion("app_name not in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameBetween(String value1, String value2) {
            addCriterion("app_name between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotBetween(String value1, String value2) {
            addCriterion("app_name not between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAppCodeIsNull() {
            addCriterion("app_code is null");
            return (Criteria) this;
        }

        public Criteria andAppCodeIsNotNull() {
            addCriterion("app_code is not null");
            return (Criteria) this;
        }

        public Criteria andAppCodeEqualTo(String value) {
            addCriterion("app_code =", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeNotEqualTo(String value) {
            addCriterion("app_code <>", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeGreaterThan(String value) {
            addCriterion("app_code >", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeGreaterThanOrEqualTo(String value) {
            addCriterion("app_code >=", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeLessThan(String value) {
            addCriterion("app_code <", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeLessThanOrEqualTo(String value) {
            addCriterion("app_code <=", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeLike(String value) {
            addCriterion("app_code like", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeNotLike(String value) {
            addCriterion("app_code not like", value, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeIn(List<String> values) {
            addCriterion("app_code in", values, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeNotIn(List<String> values) {
            addCriterion("app_code not in", values, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeBetween(String value1, String value2) {
            addCriterion("app_code between", value1, value2, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppCodeNotBetween(String value1, String value2) {
            addCriterion("app_code not between", value1, value2, "appCode");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlIsNull() {
            addCriterion("app_img_url is null");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlIsNotNull() {
            addCriterion("app_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlEqualTo(String value) {
            addCriterion("app_img_url =", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlNotEqualTo(String value) {
            addCriterion("app_img_url <>", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlGreaterThan(String value) {
            addCriterion("app_img_url >", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("app_img_url >=", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlLessThan(String value) {
            addCriterion("app_img_url <", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlLessThanOrEqualTo(String value) {
            addCriterion("app_img_url <=", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlLike(String value) {
            addCriterion("app_img_url like", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlNotLike(String value) {
            addCriterion("app_img_url not like", value, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlIn(List<String> values) {
            addCriterion("app_img_url in", values, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlNotIn(List<String> values) {
            addCriterion("app_img_url not in", values, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlBetween(String value1, String value2) {
            addCriterion("app_img_url between", value1, value2, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppImgUrlNotBetween(String value1, String value2) {
            addCriterion("app_img_url not between", value1, value2, "appImgUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlIsNull() {
            addCriterion("app_in_url is null");
            return (Criteria) this;
        }

        public Criteria andAppInUrlIsNotNull() {
            addCriterion("app_in_url is not null");
            return (Criteria) this;
        }

        public Criteria andAppInUrlEqualTo(String value) {
            addCriterion("app_in_url =", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlNotEqualTo(String value) {
            addCriterion("app_in_url <>", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlGreaterThan(String value) {
            addCriterion("app_in_url >", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlGreaterThanOrEqualTo(String value) {
            addCriterion("app_in_url >=", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlLessThan(String value) {
            addCriterion("app_in_url <", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlLessThanOrEqualTo(String value) {
            addCriterion("app_in_url <=", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlLike(String value) {
            addCriterion("app_in_url like", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlNotLike(String value) {
            addCriterion("app_in_url not like", value, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlIn(List<String> values) {
            addCriterion("app_in_url in", values, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlNotIn(List<String> values) {
            addCriterion("app_in_url not in", values, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlBetween(String value1, String value2) {
            addCriterion("app_in_url between", value1, value2, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppInUrlNotBetween(String value1, String value2) {
            addCriterion("app_in_url not between", value1, value2, "appInUrl");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersIsNull() {
            addCriterion("app_developers is null");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersIsNotNull() {
            addCriterion("app_developers is not null");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersEqualTo(String value) {
            addCriterion("app_developers =", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersNotEqualTo(String value) {
            addCriterion("app_developers <>", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersGreaterThan(String value) {
            addCriterion("app_developers >", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersGreaterThanOrEqualTo(String value) {
            addCriterion("app_developers >=", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersLessThan(String value) {
            addCriterion("app_developers <", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersLessThanOrEqualTo(String value) {
            addCriterion("app_developers <=", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersLike(String value) {
            addCriterion("app_developers like", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersNotLike(String value) {
            addCriterion("app_developers not like", value, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersIn(List<String> values) {
            addCriterion("app_developers in", values, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersNotIn(List<String> values) {
            addCriterion("app_developers not in", values, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersBetween(String value1, String value2) {
            addCriterion("app_developers between", value1, value2, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppDevelopersNotBetween(String value1, String value2) {
            addCriterion("app_developers not between", value1, value2, "appDevelopers");
            return (Criteria) this;
        }

        public Criteria andAppIsadminIsNull() {
            addCriterion("app_isadmin is null");
            return (Criteria) this;
        }

        public Criteria andAppIsadminIsNotNull() {
            addCriterion("app_isadmin is not null");
            return (Criteria) this;
        }

        public Criteria andAppIsadminEqualTo(String value) {
            addCriterion("app_isadmin =", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminNotEqualTo(String value) {
            addCriterion("app_isadmin <>", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminGreaterThan(String value) {
            addCriterion("app_isadmin >", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminGreaterThanOrEqualTo(String value) {
            addCriterion("app_isadmin >=", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminLessThan(String value) {
            addCriterion("app_isadmin <", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminLessThanOrEqualTo(String value) {
            addCriterion("app_isadmin <=", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminLike(String value) {
            addCriterion("app_isadmin like", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminNotLike(String value) {
            addCriterion("app_isadmin not like", value, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminIn(List<String> values) {
            addCriterion("app_isadmin in", values, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminNotIn(List<String> values) {
            addCriterion("app_isadmin not in", values, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminBetween(String value1, String value2) {
            addCriterion("app_isadmin between", value1, value2, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppIsadminNotBetween(String value1, String value2) {
            addCriterion("app_isadmin not between", value1, value2, "appIsadmin");
            return (Criteria) this;
        }

        public Criteria andAppOrderIsNull() {
            addCriterion("app_order is null");
            return (Criteria) this;
        }

        public Criteria andAppOrderIsNotNull() {
            addCriterion("app_order is not null");
            return (Criteria) this;
        }

        public Criteria andAppOrderEqualTo(Integer value) {
            addCriterion("app_order =", value, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderNotEqualTo(Integer value) {
            addCriterion("app_order <>", value, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderGreaterThan(Integer value) {
            addCriterion("app_order >", value, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderGreaterThanOrEqualTo(Integer value) {
            addCriterion("app_order >=", value, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderLessThan(Integer value) {
            addCriterion("app_order <", value, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderLessThanOrEqualTo(Integer value) {
            addCriterion("app_order <=", value, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderIn(List<Integer> values) {
            addCriterion("app_order in", values, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderNotIn(List<Integer> values) {
            addCriterion("app_order not in", values, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderBetween(Integer value1, Integer value2) {
            addCriterion("app_order between", value1, value2, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppOrderNotBetween(Integer value1, Integer value2) {
            addCriterion("app_order not between", value1, value2, "appOrder");
            return (Criteria) this;
        }

        public Criteria andAppStateIsNull() {
            addCriterion("app_state is null");
            return (Criteria) this;
        }

        public Criteria andAppStateIsNotNull() {
            addCriterion("app_state is not null");
            return (Criteria) this;
        }

        public Criteria andAppStateEqualTo(String value) {
            addCriterion("app_state =", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateNotEqualTo(String value) {
            addCriterion("app_state <>", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateGreaterThan(String value) {
            addCriterion("app_state >", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateGreaterThanOrEqualTo(String value) {
            addCriterion("app_state >=", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateLessThan(String value) {
            addCriterion("app_state <", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateLessThanOrEqualTo(String value) {
            addCriterion("app_state <=", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateLike(String value) {
            addCriterion("app_state like", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateNotLike(String value) {
            addCriterion("app_state not like", value, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateIn(List<String> values) {
            addCriterion("app_state in", values, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateNotIn(List<String> values) {
            addCriterion("app_state not in", values, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateBetween(String value1, String value2) {
            addCriterion("app_state between", value1, value2, "appState");
            return (Criteria) this;
        }

        public Criteria andAppStateNotBetween(String value1, String value2) {
            addCriterion("app_state not between", value1, value2, "appState");
            return (Criteria) this;
        }

        public Criteria andAppCreateIsNull() {
            addCriterion("app_create is null");
            return (Criteria) this;
        }

        public Criteria andAppCreateIsNotNull() {
            addCriterion("app_create is not null");
            return (Criteria) this;
        }

        public Criteria andAppCreateEqualTo(Date value) {
            addCriterionForJDBCTime("app_create =", value, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateNotEqualTo(Date value) {
            addCriterionForJDBCTime("app_create <>", value, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateGreaterThan(Date value) {
            addCriterionForJDBCTime("app_create >", value, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("app_create >=", value, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateLessThan(Date value) {
            addCriterionForJDBCTime("app_create <", value, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("app_create <=", value, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateIn(List<Date> values) {
            addCriterionForJDBCTime("app_create in", values, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateNotIn(List<Date> values) {
            addCriterionForJDBCTime("app_create not in", values, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("app_create between", value1, value2, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppCreateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("app_create not between", value1, value2, "appCreate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateIsNull() {
            addCriterion("app_update is null");
            return (Criteria) this;
        }

        public Criteria andAppUpdateIsNotNull() {
            addCriterion("app_update is not null");
            return (Criteria) this;
        }

        public Criteria andAppUpdateEqualTo(Date value) {
            addCriterionForJDBCTime("app_update =", value, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateNotEqualTo(Date value) {
            addCriterionForJDBCTime("app_update <>", value, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateGreaterThan(Date value) {
            addCriterionForJDBCTime("app_update >", value, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("app_update >=", value, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateLessThan(Date value) {
            addCriterionForJDBCTime("app_update <", value, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("app_update <=", value, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateIn(List<Date> values) {
            addCriterionForJDBCTime("app_update in", values, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateNotIn(List<Date> values) {
            addCriterionForJDBCTime("app_update not in", values, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("app_update between", value1, value2, "appUpdate");
            return (Criteria) this;
        }

        public Criteria andAppUpdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("app_update not between", value1, value2, "appUpdate");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}