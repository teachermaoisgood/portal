package com.hbxy.tg.portal.admin.app.service;

import com.hbxy.tg.portal.admin.app.model.Appmodle;
import com.hbxy.tg.portal.admin.app.model.AppmodleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AppmodleService {
    long countByExample(AppmodleExample example);

    int deleteByExample(AppmodleExample example);

    int deleteByPrimaryKey(Integer modeId);

    int insert(Appmodle record);

    int insertSelective(Appmodle record);

    List<Appmodle> selectByExample(AppmodleExample example);

    List<Appmodle> selectAll();

    Appmodle selectByPrimaryKey(Integer modeId);

    int updateByExampleSelective(@Param("record") Appmodle record, @Param("example") AppmodleExample example);

    int updateByExample(@Param("record") Appmodle record, @Param("example") AppmodleExample example);

    int updateByPrimaryKeySelective(Appmodle record);

    int updateByPrimaryKey(Appmodle record);
}