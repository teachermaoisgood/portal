﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page buffer="32kb" %>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>登录</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/login.css" />
</head>

<body class="beg-login-bg">
<form name="lv"  method="post">
    <div class="beg-login-box">
        <header>
            <h1>企业门户系统</h1>
        </header>
        <div class="beg-login-main">
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe612;</i>
                </label>
                <input type="text" id="userId" name="userId" lay-verify="userName" autocomplete="off" placeholder="这里输入登录名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe642;</i>
                </label>
                <input type="password" name="userPass" lay-verify="password" autocomplete="off" placeholder="这里输入密码" class="layui-input">
            </div>
            <div class="layui-form-item">
    <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text" style="font-size: 13px;color: #0C0C0C">
                ${message}
            </div>
        </div>


                <button class="layui-btn layui-btn-primary" onclick="login()">
                    <%--<button class="layui-btn layui-btn-primary" lay-submit lay-filter="login">--%>
                    <i class="layui-icon">&#xe650;</i> 登录
                </button>

                <div class="beg-clear"></div>
            </div>

        </div>

        <footer>
        </footer>
    </div>
</form>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/layui.js"></script>
<script>
    // 登陆代码
    function login() {
var userid=document.getElementById("userId").value;
if(isNaN(userid)){
    alert("用户名必须是由数字组成请检查")

}else {
    document.location.replace("${pageContext.request.contextPath}/login");

    document.lv.action = "${pageContext.request.contextPath}/login";
    document.lv.submit();
}


    }

</script>
</body>

</html>