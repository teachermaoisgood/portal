<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加人员</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        添加人员
    </legend>
</fieldset>
<!-- 内容主体区域 -->
<div style="padding: 15px;">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline">
                <input type="tel" id="user_name" lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">用户密码</label>
            <div class="layui-input-inline">
                <input type="tel" id="user_pass" lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">绑定手机</label>
            <div class="layui-input-inline">
                <input type="tel" id="user_tel" lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">绑定邮箱</label>
            <div class="layui-input-inline">
                <input type="tel" id="user_email" lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-inline">
                <input type="tel" id="user_pass_sure" lay-verify="" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <%--<div class="layui-form-item">--%>
    <%--<div class="layui-inline">--%>
    <%--<label class="layui-form-label">系统账号</label>--%>
    <%--<div class="layui-input-inline">--%>
    <%--<input type="tel" id="user_issysacc" lay-verify="" autocomplete="off" class="layui-input">--%>
    <%--</div>--%>
    <%--</div>--%>
    <%--</div>--%>


</div>
<div class="layui-form-item">
    <div class="layui-input-block">
        <button class="layui-btn" lay-submit="" lay-filter="demo1" onclick="add()">立即添加</button>
    </div>
</div>
<script language="JavaScript">
    function add() {
        var user_name = document.getElementById('user_name').value;
        var user_email = document.getElementById('user_email').value;
        var user_tel = document.getElementById('user_tel').value;
        var user_pass = document.getElementById('user_pass').value;


        if (user_name == "") {
            layui.use('layer', function () {
                layer.msg("填写名字", {icon: 2, time: 1000});
            });
            return false;
        }
        if (user_email == "") {
            layui.use('layer', function () {
                layer.msg("填写邮箱号码", {icon: 2, time: 1000});
            });
            return false;
        }
        if (user_tel == "") {
            layui.use('layer', function () {
                layer.msg("填写电话号码", {icon: 2, time: 1000});
            });
            return false;
        }
        if (user_pass == "" && user_pass != user_repas) {
            layui.use('layer', function () {
                layer.msg("填写密码", {icon: 2, time: 1000});
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/adduser",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data: JSON.stringify(
                {
                    userName: user_name,
                    userEmail: user_email,
                    userTel: user_tel,
                    userPass: user_pass,

                }
            ),
            success: function (data) {
                if (data == 1) {
                    layui.use('layer', function () {
                        layer.msg("添加成功", {icon: 1, time: 1000});
                    });
                    load("user")
                }
                else {
                    layui.use('layer', function () {
                        layer.msg("添加失败", {icon: 2, time: 1000});
                    });
                }
            }
        });
    }
</script>
</body>
</html>