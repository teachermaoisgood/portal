<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>组织管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
组织管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary" onclick="load('addorg')" >添加新组织</button>
<table class="layui-hide" id="org_table" lay-filter="org_table"></table>
<script>
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#org_table'
            , url: '${pageContext.request.contextPath}/xxx' <%--接口留出--%>
            , cols: [[
                {field: 'org_id', title: '组织编号', width: 100}
                , {field: 'org_name', title: '组织名称', edit: 'text', width: 150}
                , {field: 'org_no', title: '组织代码', edit: 'text', width: 100}
                , {field: 'org_father_id', title: '父组织名称', edit: 'text', width: 150}
                , {field: 'org_address', title: '办公地址', edit: 'text', width: 200}
                , {field: 'org_remark', title: '备注', edit: 'text', width: 200}
                , {fixed: 'org_right', title:'操作', toolbar: '#barDemo', width:150}


            ]]

        });

        //监听单元格编辑
        table.on('edit(org_table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：' + value);

            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/doUpdateCar",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("修改成功", {icon: 1, time: 1000});
                    table.reload('org_table');
                }
            });
        });


    });
</script>
</body>
</html>