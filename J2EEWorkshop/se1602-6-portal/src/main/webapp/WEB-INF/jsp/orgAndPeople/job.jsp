<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>职位管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
职位管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary" onclick="load('addjob')">添加新职位</button>
<table class="layui-hide" id="job_table" lay-filter="job_table"></table>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="add">添加人员</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#job_table'
            , url: '${pageContext.request.contextPath}/xxx' <%--接口留出--%>
            , cols: [[
                {field:'job_id',title:'职位编号', width:100}
                ,{field:'job_type',title:'职位类型', width:100,edit:'text'}
                ,{field:'job_no',title:'职位编码', width:100,edit: 'text'}
                ,{field:'job_name', title:'职位名称',width:100,edit: 'text'}
                ,{field:'job_describe', title:'职位描述',width:160,edit: 'text'}
                ,{field:'job_salary',title:'工资级别', width:160,edit: 'text'}
                ,{fixed:'job_right', title:'操作', toolbar: '#barDemo', width:150}

            ]]

        });

        //监听单元格编辑
        table.on('edit(job _table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：' + value);

            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/doUpdateCar",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("修改成功", {icon: 1, time: 1000});
                    table.reload('org_table');
                }
            });
        });


    });
</script>

</body>
</html>