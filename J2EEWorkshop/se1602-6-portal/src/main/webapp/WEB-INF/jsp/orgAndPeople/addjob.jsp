<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加职位</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
添加职位
    </legend>
</fieldset>
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">职位类型</label>
                <div class="layui-input-inline">
                    <input type="tel" name="job_type" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">职位编码</label>
                <div class="layui-input-inline">
                    <input type="tel" name="job_no" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">职位名称</label>
                <div class="layui-input-inline">
                    <input type="tel" name="job_name" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">职位描述</label>
                <div class="layui-input-inline">
                    <input type="tel" name="job_descirbe" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">工资级别</label>
                <div class="layui-input-inline">
                    <input type="tel" name="job_salary" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>


    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">立即添加</button>
        </div>
    </div>

</body>
</html>