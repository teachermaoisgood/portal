<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        用户管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary" onclick="load('adduser')">添加新用户</button>
<table class="layui-hide" id="peo_table" lay-filter="peo_table"></table>
<script type="text/html" id="userbar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="add">添加角色</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script>
    var selectPeoId;
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#peo_table'
            , url: '${pageContext.request.contextPath}/usertable' <%--接口留出--%>
            , cols: [[
                {field: 'userId', title: '人员编号', width: 100}
                , {field: 'userName', title: '用户名', width: 100, edit: 'text'}
                , {field: 'userEmail', title: '绑定邮箱', width: 150, edit: 'text'}
                , {field: 'userTel', title: '电话', width: 150, edit: 'text'}
                , {field: 'userIssysacc', title: '权限', width: 150, edit: 'text'}
                , {title: '操作', toolbar: '#userbar'}

            ]]

        });
        table.on('tool(peo_table)', function(obj){
            var q_data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('确定删除此用户？', function(index){
                    obj.del();
                    deluser(q_data);
                    layer.close(index);
                });
            }else if(obj.event === 'add'){

                selectPeoId=q_data.userId;
                load('peo_role');
            }
        });



        //监听单元格编辑
        table.on('edit(peo_table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            // layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：' + value);

            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/edituser",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("修改成功", {icon: 1, time: 1000});
                    table.reload('peo_table');
                }
            });
        });
    });
    function deluser(data) {
        console.log(data);
        $.ajax({
            type: "post",
            contentType: "application/json;charset=UTF-8",
            url: "${pageContext.request.contextPath}/deluser",
            data: JSON.stringify(data),
            success: function (data) {
                layer.msg("删除成功", {icon: 1, time: 1000});
                // table.reload('role_table');
            }
        });

    }

</script>
</body>
</html>