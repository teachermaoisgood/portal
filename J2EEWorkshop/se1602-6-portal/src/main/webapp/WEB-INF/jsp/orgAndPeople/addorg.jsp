<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加组织</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
添加组织
    </legend>
</fieldset>
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">组织名称</label>
                <div class="layui-input-inline">
                    <input type="tel" name="org_name" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">组织代码</label>
                <div class="layui-input-inline">
                    <input type="tel" name="org_no" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">父组织编号</label>
                <div class="layui-input-inline">
                    <input type="tel" name="org_father_id" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">管理员编号</label>
                <div class="layui-input-inline">
                    <input type="tel" name="org_admin_id" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">办公地址</label>
                <div class="layui-input-inline">
                    <input type="tel" name="org_address" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-inline">
                    <input type="tel" name="org_remark" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>


    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">立即添加</button>
        </div>
    </div>

</body>
</html>