<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户分配角色</title>
    <script type="text/javascript">
        // 移动id为from的列表中的选中项到id为to的列表中
        function move(from,to) {
            // 获取移动源
            var fromBox = document.getElementById(from);
            // 获取移动目的地
            var toBox = document.getElementById(to);
            // 当移动源存在选中项时
            while(fromBox.selectedIndex != -1){
                // 将移动源中的选中项添加到移动目的地的末尾
                toBox.appendChild(fromBox.options[fromBox.selectedIndex]);
            }
        }

        // 移动id为from的列表中的所有项到id为to的列表中
        function moveAll(from,to) {
            // 获取移动源
            var fromBox = document.getElementById(from);
            // 获取移动目的地
            var toBox = document.getElementById(to);
            // 当移动源存在选中项时
            while(fromBox.options.length > 0){
                // 将移动源中的选中项添加到移动目的地的末尾
                toBox.appendChild(fromBox.options[0]);
            }
        }
    </script>
</head>

<div style="padding: 15px;" class="content">
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
角色权限分配
    </legend>
</fieldset>
<table style="padding: 15px;">
    <tr>
        <td>
<select id="leftBox" name="allright" size="5" multiple="multiple" style="width: 200px">
    <option value="0" disabled>全部角色</option>
    <c:forEach var="role" items="${rolelist}" varStatus="vstatus">

    <option value="${role.roleId}">${role.roleName}</option>

    </c:forEach>


</select>
        </td>
        <td>

<div>
    <ul><input onclick="move('leftBox','rightBox')" class="layui-btn" type="button" value="添加" style="float: left"></ul>
    <ul> <input onclick="move('rightBox','leftBox')"  class="layui-btn" type="button" value="移除" ></ul>

</div>
        </td>
        <td>
<select id="rightBox" name="hasright" size="5" multiple="multiple" style="width: 200px">
    <option value="" disabled>已有角色</option>
</select>
        </td>
    </tr>
</table>
</div>
<div class="layui-form-item">
    <div class="layui-input-block">
        <button class="layui-btn" lay-submit="" lay-filter="demo1"onclick="role_right()">立即提交</button>
    </div>
</div>

<script>
    //Demo
    //layui.use('form', function(){
      //  var form = layui.form;
        $(function() {
            $("#addroleRight").onclick(function () {
                var roleRightIdoptions = $('#roleRightId option');
                var options = $('#nosetrole option:selected');
                for (i = 0; i < options.length; i++) {
                    var value = options[i].value;
                    var text = options[i].text;
                    //添加一项option
                    $("#roleRightId").append("<option value='" + value + "'>" + text + "</option>");
                    //移除一项option
                    $("#nosetroleRight option[value='" + value + "']").remove()
                }

        });
    })

    $(function() {
        $("#delroleRight").click(function () {
            var options = $('#roleRightId option:selected');
            //遍历
            for (i = 0; i < options.length; i++) {
                var value = options[i].value;
               // var text = options[i].text;
                //添加一项option
                $("#nosetroleRight").append("<option value='" + value + "'>" + text + "</option>");
                //移除一项option
                $("#roleRightId option[value='" + value + "']").remove()
            }

    })


    });
    function role_right() {
        var options = $('#rightBox option');
        var data= new Array();
        //数组第一位是id+n个权限
        data.push(selectPeoId);
        for (var i = 1; i < options.length; i++) {

             var f= options[i].value;

            data.push(f);
        }



        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/peo_role",
            // contentType: "application/json;charset=UTF-8",
            // dataType: "json",
            traditional:true,
            data: {
              data:data
            },
            success: function (data) {
                if (data){
                    alert(data);
                }
            }
        });

    }

</script>
</body>
</html>