<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>职级管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
职级管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary"onclick="load('addjoblevel')">添加新职级</button>

<table class="layui-hide" id="joblev_table" lay-filter="joblev_table"></table>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="add">升级</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">降级</a>
</script>
<script>
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#joblev_table'
            , url: '${pageContext.request.contextPath}/xxx' <%--接口留出--%>
            , cols: [[
                {field:'lev_id',title:'职级编号', width:100}
                ,{field:'lev_name',title:'职级名称', width:100,edit:'text'}
                ,{field:'lev_no', title:'职级编码',width:100,edit: 'text'}
                ,{field:'lev_remark',title:'备注', width:100,edit: 'text'}
                ,{fixed: 'lev_right', title:'操作', toolbar: '#barDemo', width:150}

            ]]

        });

        //监听单元格编辑
        table.on('edit(joblev_table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            layer.msg('[ID: ' + data.id + '] ' + field + ' 字段更改为：' + value);

            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("修改成功", {icon: 1, time: 1000});
                    table.reload('org_table');
                }
            });
        });


    });

</script>

</body>
</html>