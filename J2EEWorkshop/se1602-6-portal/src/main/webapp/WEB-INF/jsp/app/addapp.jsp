<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
添加应用
    </legend>
</fieldset>
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">应用名称</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_name" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">应用代码</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_code" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">图标地址</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_img_url" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">应用地址</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_in_url" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开发商</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_dev" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">系统应用</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_isadmin" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_state" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="demo1" onclick="addapp()">立即添加</button>
        </div>
    </div>
<script language="JavaScript">

    function addapp() {
        var app_name        =document.getElementById('app_name').value;
        var app_code        =document.getElementById('app_code').value;
        var app_img_url     =document.getElementById('app_img_url').value;
        var app_in_url      =document.getElementById('app_in_url').value;
        var app_isadmin     =document.getElementById('app_isadmin').value;
        var app_state       =document.getElementById('app_state').value;
        var app_dev         =document.getElementById('app_dev').value;
        if (app_name == "") {
            layui.use('layer', function(){
                layer.msg("填写名字",{icon: 2,time:1000});
            });
            return false;
        }
        if (app_code == "") {
            layui.use('layer', function(){
                layer.msg("填写代码",{icon: 2,time:1000});
            });
            return false;
        }
        if (app_img_url == "") {
            layui.use('layer', function(){
                layer.msg("填写图标地址",{icon: 2,time:1000});
            });
            return false;
        }
        if (app_in_url == "") {
            layui.use('layer', function(){
                layer.msg("填写应用地址",{icon: 2,time:1000});
            });
            return false;
        }
        if (app_dev == "") {
            layui.use('layer', function(){
                layer.msg("填写开发商",{icon: 2,time:1000});
            });
            return false;
        }
        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/app/addapp",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data: JSON.stringify(
                {
                    appName:            app_name    ,
                    appCode:            app_code    ,
                    appImgUrl:          app_img_url ,
                    appInUrl:           app_in_url  ,
                    appDevelopers:     app_dev ,
                    appIsadmin:        app_isadmin    ,
                    appState:            app_state     ,

                }
            ),
            success: function (data) {
                if(data==1){
                    layui.use('layer', function(){
                        layer.msg("添加成功",{icon: 1,time:1000});
                    });
                    load('app');

                }
                else {
                    layui.use('layer', function(){
                        layer.msg("添加失败",{icon: 2,time:1000});
                    });

                }


            }
        });

    }
</script>
</body>
</html>