<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>应用管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        应用管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary" onclick="load('addapp')">添加新应用</button>
<table class="layui-hide" id="app_table" lay-filter="app_table"></table>
<script type="text/html" id="appbar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="handle">管理模块</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script language="JavaScript">




    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#app_table'
            , url: '${pageContext.request.contextPath}/app/apptable' <%--接口留出--%>
            , cols: [[
                {field:'appId',title:'应用编号', width:100}
                ,{field:'appName', title:'应用名称',width:100,edit:'text'}
                ,{field:'appState', title:'状态',width:100,edit: 'text'}
                ,{field:'appDevelopers', title:'开发商',width:100,edit: 'text'}
                 ,{ title:'操作', toolbar: '#appbar'}


            ]]

        });

        table.on('tool(app_table)', function(obj){
            var q_data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    appdel(q_data);
                    layer.close(index);
                });
            } else if(obj.event === 'handle'){
                load('model');
            }
        });
        //删除
        function appdel(data) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/app/appdel",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("删除成功", {icon: 1, time: 1000});
                    // table.reload('role_table');
                }
            });
        }
        //监听单元格编辑
        table.on('edit(app_table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            layer.msg('[ID: ' + data.app_id + '] ' + field + ' 字段更改为：' + value);
            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/app/appedit",
                data: JSON.stringify(data),
                dataType:"json",
                success: function (data) {
                    layer.msg("修改成功", {icon: 1, time: 1000});
                    table.reload('org_table');
                }
            });
        });
    });
</script>

</body>
</html>