<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
添加模块
    </legend>
</fieldset>
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">应用编号</label>
                <div class="layui-input-inline">
                    <input type="tel" id="app_id" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">模块名称</label>
                <div class="layui-input-inline">
                    <input type="tel" id="modle_name" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">模块代码</label>
                <div class="layui-input-inline">
                    <input type="tel" id="modle_code" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">图片地址</label>
                <div class="layui-input-inline">
                    <input type="tel" id="modle_img_url" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">模块入口</label>
                <div class="layui-input-inline">
                    <input type="tel" id="modle_in_url" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">系统应用</label>
                <div class="layui-input-inline">
                    <input type="tel" id="modle_isadmin" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-inline">
                    <input type="tel" id="modle_state" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>


    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="demo1"onclick="addmod()">立即添加</button>
        </div>
    </div>
<script language="JavaScript">

    function addmod() {
        var modle_name        =document.getElementById('modle_name').value;
        var app_id            =document.getElementById('app_id').value;
        var modle_code        =document.getElementById('modle_code').value;
        var modle_img_url     =document.getElementById('modle_img_url').value;
        var modle_in_url      =document.getElementById('modle_in_url').value;
        var modle_isadmin     =document.getElementById('modle_isadmin').value;
        var modle_state       =document.getElementById('modle_state').value;
        if (modle_name == "") {
            layui.use('layer', function(){
                layer.msg("填写名字",{icon: 2,time:1000});
            });
            return false;
        }
        if (modle_code == "") {
            layui.use('layer', function(){
                layer.msg("填写代码",{icon: 2,time:1000});
            });
            return false;
        }
        if (modle_img_url == "") {
            layui.use('layer', function(){
                layer.msg("填写图标地址",{icon: 2,time:1000});
            });
            return false;
        }if (modle_in_url == "") {
            layui.use('layer', function(){
                layer.msg("填写模块地址",{icon: 2,time:1000});
            });
            return false;
        }

        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/modle/addmod",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data: JSON.stringify(
                {
                    appId:                app_id,
                    modleName:              modle_name    ,
                    modleCode:            modle_code    ,
                    modleImgUrl:          modle_img_url ,
                    modleInUrl:           modle_in_url  ,
                    modleIsadmin:         modle_isadmin    ,
                    modelState:          modle_state     ,

                }
            ),
            success: function (data) {
                if(data==1){
                    layui.use('layer', function(){
                        layer.msg("添加成功",{icon: 1,time:1000});
                    });
                    load('model');

                }
                else {
                    layui.use('layer', function(){
                        layer.msg("添加失败",{icon: 2,time:1000});
                    });

                }


            },
            error:function () {
                alert(0);

            }
        });

    }
</script>

</body>
</html>