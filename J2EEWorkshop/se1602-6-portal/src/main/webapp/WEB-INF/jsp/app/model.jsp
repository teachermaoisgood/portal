<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>模块管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        模块管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary" onclick="load('addmodel')">添加模块</button>
<table class="layui-hide" id="model_table" lay-filter="model_table"></table>
<script type="text/html" id="modelbar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script>
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#model_table'
            , url: '${pageContext.request.contextPath}/modle/modletable' <%--接口留出--%>
            , cols: [[
                {field:'modleId',title:'模块编号', width:100}
                ,{field:'modleName', title:'模块名称',width:100,edit:'text'}
                ,{field:'modelState', title:'模块状态',width:100,edit: 'text'}
                ,{field:'appId', title:'所属应用编号',width:100,edit: 'text'}
                ,{ title:'操作', toolbar: '#modelbar'}

            ]]

        });
        table.on('tool(model_table)', function(obj){
            var q_data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    delmodle(q_data);
                     layer.close(index);
                });
            }
        });
        function delmodle(data) {
            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/modle/moddel",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("删除成功", {icon: 1, time: 1000});
                    // table.reload('role_table');
                }
            });

        }

        //监听单元格编辑
        table.on('edit(model_table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            layer.msg('[ID: ' + data.modleId + '] ' + field + ' 字段更改为：' + value);
            $.ajax({
                type: "post",
                contentType: "application/json;charset=UTF-8",
                url: "${pageContext.request.contextPath}/modle/modedit",
                data: JSON.stringify(data),
                success: function (data) {
                    layer.msg("修改成功", {icon: 1, time: 1000});
                    table.reload('model_table');
                }
            });
        });


    });

</script>

</body>
</html>