<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>企业门户应用管理</title>
    <link rel="stylesheet" href="static/css/layui.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>


</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">企业门户应用管理</div>
        <ul class="layui-nav layui-layout-left">

        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    ${username}
                </a>

            </li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">组织人员管理</a>
                    <dl class="layui-nav-child">
                        <%--<dd><a href="javascript:load('org');">组织管理</a></dd>--%>
                        <dd><a href="javascript:load('user');">用户管理</a></dd>
                        <%--<dd><a href="javascript:load('job');">职务管理</a></dd>--%>
                        <%--<dd><a href="javascript:load('lev');">职级管理</a></dd>--%>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">应用管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:load('app');">应用管理</a></dd>
                        <dd><a href="javascript:load('model');">模块管理</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">权限管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:load('addrole');">添加角色</a></dd>
                        <dd><a href="javascript:load('role');">角色管理</a></dd>
                        <%--<dd><a href="javascript:load('right');">授权管理</a></dd>--%>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">个人中心</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:lod('peo');">个人信息</a></dd>
                        <dd><a href="javascript:lod('editpass');">修改密码</a></dd>
                    </dl>
                </li>

            </ul>
        </div>
    </div>

    <div class="layui-body" id="body">
        <!-- 内容主体区域 -->
        <%--<div style="padding: 15px;" class="content">内容主体区域</div>--%>
        <div id="content">
            <fieldset class="layui-elem-field site-demo-button" style="margin-top: 30px;">
                <legend>大事件</legend>
            <ul class="layui-timeline">
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">4月24日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="100%"></div>
                        </div>
                        <p>
                            竣工
                        <ul>
                            <li>感谢毛老师谆谆教导</li>
                            <li>感谢，感谢</li>
                        </ul>
                        </p>
                        
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">4月23日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="90%"></div>
                        </div>
                        <p>
                            进行最后一次整体测试
                        <ul>
                            <li>无异常</li>
                        </ul>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">4月20日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="80%"></div>
                        </div>
                        <p>
                            完成全部模块功能
                        <ul>
                            <li>人员管理</li>
                            <li>应用管理</li>
                            <li>权限管理</li>
                            <li>个人中心</li>
                        </ul>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">4月16日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="70%"></div>
                        </div>
                        <p>
                            拦截器功能上线
                        <ul>
                            <li>拦截除login所有操作</li>
                        </ul>
                        </p>
                        

                    </div>
                </li>
                <li class="layui-timeline-item">
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">4月11日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="60%"></div>
                        </div>
                        <p>
                            前后端通讯完成
                        <ul>
                            <li>前端-后端-数据库</li>
                        </ul>
                        <ul>
                            <li>html+js+ajax+springBoot+Mysql</li>
                        </ul>
                        </p>
                        
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">3月26日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="40%"></div>
                        </div>
                        <p>
                            提交数据库模型
                        <ul>
                            <li>sql语句</li>
                        </ul>
                        <ul>
                            <li>同日全体组构建数据库成功</li>
                        </ul>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">3月22日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="30%"></div>
                        </div>
                        <p>
                            首次上传前端代码
                        <ul>
                            <li>采用layui框架</li>
                        </ul>
                        <ul>
                            <li>轻量、易用</li>
                        </ul>
                        </p>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">3月20日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="20%"></div>
                        </div>
                        <p>
                            第三次组内会议
                        <ul>
                            <li>确定前端框架后端框架</li>
                        </ul>
                        </p>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">3月18日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="15%"></div>
                        </div>
                        <p>
                            第二次组内会议
                        <ul>
                        <li>小组任务分配</li>
                            <li>取长补短，各司其职</li>
                        </ul>
                                    </p>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">3月17日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="10%"></div>
                        </div>
                        <p>第一次组内会议</p>
                        <ul>
                            <li>确定项目方向</li>
                            <li>制定详细计划</li>
                        </ul>
                        </p>
                        
                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>
                    <div class="layui-timeline-content layui-text">
                        <h3 class="layui-timeline-title">3月14日</h3>
                        <div class="layui-progress" style="width: 50%;margin: 0 auto">
                            <div class="layui-progress-bar" lay-percent="0%"></div>
                        </div>
                        <p>
                            tg项目小组成立

                        <ul>
                        <li>甘宗剑</li>
                        <li>孙英鹤</li>
                        <li>任泽华</li>
                        <li>颜天昊</li>
                        <li>王熙伟</li>
                    </ul>
                        </p>

                    </div>
                </li>
                <li class="layui-timeline-item">
                    <i class="layui-icon layui-timeline-axis"></i>

                </li>
            </ul>
            </fieldset>



        </div>
    </div>
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © 软件1602第六小组
        <a style="float: right" href="${pageContext.request.contextPath }/logout">退出</a>

    </div>
</div>
<script src="/static/layui.js"></script>
<script>
    var username="${username}";
    //JavaScript代码区域
    layui.use('element',function(){
        var element = layui.element;


    });
    function load(page) {
        if("${userss}"=="1"){
            if(page){
                $.ajax({
                    dataType: "text",
                    url: "${pageContext.request.contextPath}/load",
                    data:{
                        page:page
                    },
                    success: function (data) {
                        $('#content').html(data);
                    }
                });
            }
            //document.location.replace("${pageContext.request.contextPath}/load"+page);
        }else {
            layui.use('layer', function(){
                layer.msg("对不起您的权限不够",{icon: 2,time:1000});
            });
        }


    }
    function lod(page) {
            if(page){
                $.ajax({
                    dataType: "text",
                    url: "${pageContext.request.contextPath}/load",
                    data:{
                        page:page
                    },
                    success: function (data) {
                        $('#content').html(data);
                    }
                });
            }




    }

</script>
</body>
</html>