<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>角色管理</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
角色管理
    </legend>
</fieldset>
<button class="layui-btn layui-btn-primary" onclick="load('addrole')">添加新角色</button>
<table class="layui-hide" id="role_table" lay-filter="role_table"></table>
<script type="text/html" id="rolebar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="toRight">分配权限</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>

<script>
    var data;
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            elem: '#role_table'
            , url: '${pageContext.request.contextPath}/right/roletable' <%--接口留出--%>
            , cols: [[
                {field: 'roleId' ,title:'角色编号' }
                ,{field:'roleName', title:'角色名称',edit: 'text' }
                ,{field:'roleCode', title:'角色代码',edit: 'text'}
                ,{field:'roleDes', title:'角色描述' ,edit: 'text' }
                ,{ title:'操作', toolbar: '#rolebar'}


            ]]

        });
        table.on('tool(role_table)', function(obj){
            q_data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){

                layer.confirm('真的删除行么', function(index){
                    obj.del();
                    delrole(q_data);
                    layer.close(index);
                });
            } else if(obj.event === 'toRight'){
                load('right');
            }
        });

        //监听单元格编辑
        table.on('edit(role_table)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            layer.msg('[ID: ' + data.roleId + '] ' + field + ' 字段更改为：' + value);
            <%--$.ajax({--%>
                <%--type: "post",--%>
                <%--contentType: "application/json;charset=UTF-8",--%>
                <%--url: "${pageContext.request.contextPath}/right/editrole",--%>
                <%--data: JSON.stringify(data),--%>
                <%--success: function (data) {--%>
                    <%--layer.msg("修改成功", {icon: 1, time: 1000});--%>
                    <%--// table.reload('role_table');--%>
                <%--}--%>
            <%--});--%>
        });


    });
    function delrole(data) {
        $.ajax({
            type: "post",
            contentType: "application/json;charset=UTF-8",
            url: "${pageContext.request.contextPath}/right/delrole",
            data: JSON.stringify(data),
            success: function (data) {
                layer.msg("删除成功", {icon: 1, time: 1000});
                // table.reload('role_table');
            }
        });

    }
</script>

</body>
</html>