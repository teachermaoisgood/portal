<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加角色</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
添加角色
    </legend>
</fieldset>
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">角色名称</label>
                <div class="layui-input-inline">
                    <input type="tel" name="role_name" id="role_name" lay-verify=""  autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">角色代码</label>
                <div class="layui-input-inline">
                    <input type="tel" name="role_code" lay-verify="" id="role_code" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">角色描述</label>
                <div class="layui-input-inline">
                    <input type="tel" name="role_des" lay-verify="" id="role_des" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>


    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit="" lay-filter="demo1" onclick="addrole()">立即添加</button>
        </div>
    </div>
<SCRIPT language="JavaScript">
    function addrole() {
        var role_name=document.getElementById('role_name').value;
        var role_code=document.getElementById('role_code').value;
        var role_des=document.getElementById('role_des').value;
        if (role_name == "") {
            layui.use('layer', function(){
                layer.msg("填写名字",{icon: 2,time:1000});
            });
            return false;
        }
        if (role_code == "") {
            layui.use('layer', function(){
                layer.msg("填写代码",{icon: 2,time:1000});
            });
            return false;
        }
        if (role_des == "") {
            layui.use('layer', function(){
                layer.msg("填写描述",{icon: 2,time:1000});
            });
            return false;
        }
        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/right/addrole",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data: JSON.stringify(
                {roleName: role_name,
                roleCode: role_code,
                roleDes: role_des
                }
            ),
            success: function (data) {
                if(data==1){
                    layui.use('layer', function(){
                        layer.msg("添加成功",{icon: 1,time:1000});
                    });
                    load('role');
                }
                else {
                    layui.use('layer', function(){
                        layer.msg("添加失败",{icon: 2,time:1000});
                    });

                }


                }
            });

    }
</SCRIPT>


</body>
</html>