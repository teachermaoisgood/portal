<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>个人信息</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        个人信息
    </legend>
</fieldset>
<div style="padding: 15px;">
    <div class="layui-form-item">
    <label class="layui-form-label" style="width: auto">
        id：${user.userId}
    </label>
    </div>
    <div class="layui-form-item">
    <label class="layui-form-label" style="width: auto">
        用户名：${user.userName}
    </label>
    </div>
    <div class="layui-form-item">
    <label class="layui-form-label" style="width: auto">
        绑定邮箱：${user.userEmail}
    </label>
    </div>
    <div class="layui-form-item">
    <label class="layui-form-label" style="width: auto">
        绑定手机号：${user.userTel}
    </label>
    </div>
</div>
</body>
</html>