<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改密码</title>
</head>
<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>
        修改密码
    </legend>
</fieldset>
    <!-- 内容主体区域 -->
    <div style="padding: 15px;">
        <div class="layui-form-item">
            <label class="layui-form-label">输入新密码</label>
            <div class="layui-input-inline">
                <input type="password" id="newpass" lay-verify="pass"  autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认新密码</label>
            <div class="layui-input-inline">
                <input type="password" id="surepass" lay-verify="pass"  autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
        </div>

    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" onclick="editpass();" lay-filter="demo1">立即修改</button>
        </div>
    </div>

</body>
</html>

<script language="JavaScript">

    function editpass() {
        var newpass=document.getElementById('newpass').value;
        var surepass=document.getElementById('surepass').value;
        if(newpass!=surepass){
            layui.use('layer', function(){
                layer.msg("两次密码不一致",{icon: 2,time:1000});
            });
            return false;
        }
        if(newpass==""||surepass==""){
            layui.use('layer', function(){
                layer.msg("请输入密码",{icon: 2,time:1000});
            });
            return false;
        }
        $.ajax({
            type: "post",
            url: "${pageContext.request.contextPath}/editpass",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data: JSON.stringify(
                {
                    userId:username,
                    userPass:newpass,

                }
            ),
            success: function (data) {
                if(data==1){
                    layui.use('layer', function(){
                        layer.msg("添加成功",{icon: 1,time:1000});
                    });
                    load('peo');

                }

            },
            error:function () {
                layui.use('layer', function(){
                    layer.msg("添加失败",{icon: 2,time:1000});
                });
            }
        });

    }
</script>